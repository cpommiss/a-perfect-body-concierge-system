<?php
/*
Plugin Name: A Perfect Body - Concierge
Plugin URI: http://www.aperfectbody.com/concierge/
Description: Implements the Concierge portion of the A Perfect Body website.
Version: 1.0
*/

require_once '_include/admin-accounts.php';
require_once '_include/admin-markets.php';
require_once '_include/admin-procedure-types.php';
require_once '_include/admin-procedures.php';
require_once '_include/admin-procedures-price-overrides.php';
require_once '_include/admin-doctors.php';
//require_once '_include/admin-financing.php';
require_once '_include/swift/swift_required.php';

class APB_Concierge {
	private $db;

	public $datasets					= array();
	
	/*
	==========================================================================
	CONSTRUCTOR
	==========================================================================	
	*/
	
	/**
	 * Initializes the plugin by setting localization, filters, and administration functions.
	 */
	function __construct() {
		global $wpdb;
		
		$this->db			= &$wpdb;
		
		load_plugin_textdomain( 'apb-concierge-locale', false, dirname( plugin_basename( __FILE__ ) ) . '/_lang' );
		
		// register initialization action
		add_action(	'init', array( $this, 'initialize' ) );
		add_action( 'admin_init', array( $this, 'admin_initialize' ) );
		add_action( 'admin_menu', array( $this, 'admin_menu') );
		add_action( 'admin_head', array( $this, 'admin_header') );
		
		// Register styles and scripts
		add_action( 'wp_print_styles', array( $this, 'register_plugin_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'register_plugin_scripts' ) );

		// Register admin styles and scripts
		add_action( 'admin_print_styles', array( $this, 'register_admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_scripts' ) );

		register_activation_hook( __FILE__, array( $this, 'activate' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );
		
		// AJAX: logout
		add_action( 'wp_ajax_apb_concierge_logout', array( $this, 'logout' ) );
		add_action( 'wp_ajax_nopriv_apb_concierge_logout', array( $this, 'logout' ) );

		// AJAX: fetch appointments
		add_action( 'wp_ajax_apb_concierge_appointments_fetch', array( $this, 'fetch_appointments' ) );
		add_action( 'wp_ajax_nopriv_apb_concierge_appointments_fetch', array( $this, 'fetch_appointments' ) );

		// AJAX: fetch appointment details
		add_action( 'wp_ajax_apb_concierge_fetch_appointment_details', array( $this, 'fetch_appointment_details' ) );
		add_action( 'wp_ajax_nopriv_apb_concierge_fetch_appointment_details', array( $this, 'fetch_appointment_details' ) );

		// AJAX: create appointment
		add_action( 'wp_ajax_apb_concierge_create_appointment', array( $this, 'create_appointment' ) );
		add_action( 'wp_ajax_nopriv_apb_concierge_create_appointment', array( $this, 'create_appointment' ) );

		// AJAX: update appointment
		add_action( 'wp_ajax_apb_concierge_update_appointment', array( $this, 'update_appointment' ) );
		add_action( 'wp_ajax_nopriv_apb_concierge_update_appointment', array( $this, 'update_appointment' ) );

		// AJAX: update appointment times
		add_action( 'wp_ajax_apb_concierge_update_appointment_time', array( $this, 'update_appointment_time' ) );
		add_action( 'wp_ajax_nopriv_apb_concierge_update_appointment_time', array( $this, 'update_appointment_time' ) );

		// AJAX: delete appointment
		add_action( 'wp_ajax_apb_concierge_remove_appointment', array( $this, 'remove_appointment' ) );
		add_action( 'wp_ajax_nopriv_apb_concierge_remove_appointment', array( $this, 'remove_appointment' ) );

		// AJAX: send e-mail reminder
		add_action( 'wp_ajax_apb_concierge_send_email_reminder', array( $this, 'send_email_reminder' ) );
		add_action( 'wp_ajax_nopriv_apb_concierge_send_email_reminder', array( $this, 'send_email_reminder' ) );
	}
	
	/**
	 * Fired when the plugin is activated.
	 *
	 * @params	$network_wide	True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog 
	 */
	public function activate( $network_wide ) {
		
	}
	
	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @params	$network_wide	True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog 
	 */
	public function deactivate( $network_wide ) {
		
	}
	
	/**
	 * Registers and enqueues admin-specific styles.
	 */
	public function register_admin_styles() {
		wp_enqueue_style( 'apb-concierge-admin-styles-base', plugins_url( 'apb-concierge/_css/admin/admin.css' ) );
	}

	/**
	 * Registers and enqueues admin-specific JavaScript.
	 */	
	public function register_admin_scripts() {
		wp_enqueue_script( 'suggest' );
		wp_enqueue_script( 'apb-concierge-admin-script-base', plugins_url( 'apb-concierge/_js/admin/admin.js' ) );
		wp_enqueue_script( 'apb-concierge-admin-script-init', plugins_url( 'apb-concierge/_js/admin/admin.init.js' ) );
	}
	
	/**
	 * Registers and enqueues plugin-specific styles.
	 */
	public function register_plugin_styles() {
		/*
		wp_enqueue_style( 'apb-concierge-plugin-styles-bootstrap', plugins_url( 'apb-concierge/_css/vendor/bootstrap.min.css' ) );
		wp_enqueue_style( 'apb-concierge-plugin-styles-bootstrap-rwd', plugins_url( 'apb-concierge/_css/vendor/bootstrap-responsive.min.css' ) );
		wp_enqueue_style( 'apb-concierge-plugin-styles-bootstrap-modal', plugins_url( 'apb-concierge/_css/vendor/bootstrap-modal.css' ) );
		wp_enqueue_style( 'apb-concierge-plugin-styles-base', plugins_url( 'apb-concierge/_css/vendor/base-admin.css' ) );
		wp_enqueue_style( 'apb-concierge-plugin-styles-base-rwd', plugins_url( 'apb-concierge/_css/vendor/base-admin-responsive.css' ) );
		wp_enqueue_style( 'apb-concierge-plugin-styles-base-glyphs', plugins_url( 'apb-concierge/_css/vendor/base-admin-fonts.css' ) );
		wp_enqueue_style( 'apb-concierge-plugin-styles-fullcalendar', plugins_url( 'apb-concierge/_css/vendor/fullcalendar.css' ) );
		wp_enqueue_style( 'apb-concierge-plugin-styles-framework', plugins_url( 'apb-concierge/_css/index.css' ) );
		*/
	}
	
	/**
	 * Registers and enqueues plugin-specific scripts.
	 */
	public function register_plugin_scripts() {
		/*
		// top of page
		wp_enqueue_script( 'apb-concierge-plugin-modernizr', plugins_url( 'apb-concierge/_js/vendor/modernizr-2.6.2.min.js' ) );
		
		// bottom of page
		wp_enqueue_script( 'apb-concierge-plugin-jquery', plugins_url( 'apb-concierge/_js/vendor/jquery-1.8.1.min.js' ), false, false, true );
		wp_enqueue_script( 'apb-concierge-plugin-jqui', plugins_url( 'apb-concierge/_js/vendor/jquery-ui-1.8.23.custom.min.js' ), false, false, true );
		wp_enqueue_script( 'apb-concierge-plugin-bootstrap', plugins_url( 'apb-concierge/_js/vendor/bootstrap.min.js' ), false, false, true );
		wp_enqueue_script( 'apb-concierge-plugin-bootstrap-modal', plugins_url( 'apb-concierge/_js/vendor/bootstrap-modal.js' ), false, false, true );
		wp_enqueue_script( 'apb-concierge-plugin-bootstrap-modalmanager', plugins_url( 'apb-concierge/_js/vendor/bootstrap-modalmanager.js' ), false, false, true );
		wp_enqueue_script( 'apb-concierge-plugin-bootstrap-timepicker', plugins_url( 'apb-concierge/_js/vendor/bootstrap-timepicker.js' ), false, false, true );
		wp_enqueue_script( 'apb-concierge-plugin-momentjs', plugins_url( 'apb-concierge/_js/vendor/moment.min.js' ), false, false, true );
		wp_enqueue_script( 'apb-concierge-plugin-fullcalendar', plugins_url( 'apb-concierge/_js/vendor/fullcalendar.min.js' ), false, false, true );
		wp_enqueue_script( 'apb-concierge-plugin-qtip', plugins_url( 'apb-concierge/_js/vendor/jquery.qtip-1.0.0-rc3.min.js' ), false, false, true );
		wp_enqueue_script( 'apb-concierge-plugin-framework', plugins_url( 'apb-concierge/_js/framework.js' ), false, false, true );
		wp_enqueue_script( 'apb-concierge-plugin-framework-init', plugins_url( 'apb-concierge/_js/framework.init.js' ), false, false, true );
		*/
	}

	/*	
	==========================================================================
	INITIALIZATION METHODS
	==========================================================================	
	*/
	
	/**
	 * Runs when the page is initialized.
	 */
	public function initialize() {
		session_start();
		
		if (!self::is_logged_in()) {
			if (stripos($_SERVER['REQUEST_URI'], '/concierge') !== false) {
				if (stripos($_SERVER['REQUEST_URI'], 'signin') === false) {
					// TODO: allow configuration settings in the WP backend to "pick" the concierge login page
					header('Location: ' . get_bloginfo('url') . '/concierge/signin/');
					die();
				} else {
					$this->authenticate();
				}
			}
		}
	}

	/**
	 * Runs when admin pages are initialized.
	 */
	public function admin_initialize() {
	}

	/**
	 * Runs when admin menus are initialized.
	 */
	public function admin_menu() {
		/* Concierge Top-Level */
		add_menu_page(	APB_Concierge_Admin_Accounts::APB_MODULE_TITLE, 
						APB_Concierge_Admin_Accounts::APB_MODULE_TITLE, 
						'add_users', 
						APB_Concierge_Admin_Accounts::APB_MODULE_SLUG, 
						array($this, 'admin_index'), 
						plugins_url('apb-concierge/_images/calendar-share.png'), 
						50);
						
		/* Markets */
		add_submenu_page(	APB_Concierge_Admin_Accounts::APB_MODULE_SLUG, 
							APB_Concierge_Admin_Markets::APB_MODULE_TITLE, 
							APB_Concierge_Admin_Markets::APB_MODULE_TITLE, 
							'add_users', 
							APB_Concierge_Admin_Markets::APB_MODULE_SLUG, 
							array($this, 'admin_markets'));

		/* Procedure Types */
		add_submenu_page(	APB_Concierge_Admin_Accounts::APB_MODULE_SLUG, 
							APB_Concierge_Admin_ProcedureTypes::APB_MODULE_TITLE, 
							APB_Concierge_Admin_ProcedureTypes::APB_MODULE_TITLE, 
							'add_users', 
							APB_Concierge_Admin_ProcedureTypes::APB_MODULE_SLUG, 
							array($this, 'admin_procedure_types'));

		/* Procedures */
		add_submenu_page(	APB_Concierge_Admin_Accounts::APB_MODULE_SLUG, 
							APB_Concierge_Admin_Procedures::APB_MODULE_TITLE, 
							APB_Concierge_Admin_Procedures::APB_MODULE_TITLE, 
							'add_users', 
							APB_Concierge_Admin_Procedures::APB_MODULE_SLUG, 
							array($this, 'admin_procedures'));

		/* Procedures */
		add_submenu_page(	APB_Concierge_Admin_Accounts::APB_MODULE_SLUG, 
							APB_Concierge_Admin_Procedures_Price_Overrides::APB_MODULE_TITLE, 
							APB_Concierge_Admin_Procedures_Price_Overrides::APB_MODULE_TITLE, 
							'add_users', 
							APB_Concierge_Admin_Procedures_Price_Overrides::APB_MODULE_SLUG, 
							array($this, 'admin_procedures_price_overrides'));
		
		/* Doctors */
		add_submenu_page(	'concierge', 
							'Doctors', 
							'Doctors', 
							'add_users', 
							'concierge-doctors', 
							array($this, 'admin_doctors'));
		
		/* Financing Terms */
		/*
		add_submenu_page(	'concierge', 
							'Financing Terms', 
							'Financing Terms', 
							'add_users', 
							'concierge-financing-terms', 
							array($this, 'admin_financing_terms'));
		*/
	}
	
	/**
	 * Runs when admin pages are initialized.
	 */
	public function admin_header() {
	    global $current_screen;
    }
	
	public function startup() {
		if (self::is_logged_in()) {
			$this->datasets			= array(
				'doctors'					=> self::get_doctors(), 
				'procedures_types'			=> self::get_procedures_types(), 
				'procedures'				=> self::get_procedures(), 
				'procedure_price_overrides'	=> self::get_procedures_price_overrides(), 
				'markets'					=> self::get_markets(self::get_userdata('id'), self::get_userdata('type')), 
				'concierges'				=> self::get_concierges(self::get_userdata('id'), self::get_userdata('type')), 
				'concierges_markets'		=> self::get_concierges_markets(self::get_userdata('id'), self::get_userdata('type'))
			);
		} else {
			$this->datasets			= array(
				'doctors'					=> self::get_doctors(), 
				'procedures_types'			=> self::get_procedures_types(), 
				'procedures'				=> self::get_procedures(), 
				'procedure_price_overrides'	=> self::get_procedures_price_overrides()
			);
		}
	}
	
	/*	
	==========================================================================
	LOGIN METHODS
	==========================================================================	
	*/
	public function authenticate() {
		if ((isset($_POST['action'])) && 
			(isset($_POST['strUsername'])) && 
			(isset($_POST['strPassword'])) && 
			(!isset($_SESSION['boolAPBC_Logon'])) && 
			(strtolower($_POST['action']) == 'authenticate')) {

			$strUsername				= stripslashes($_POST['strUsername']);
			$strPassword				= stripslashes($_POST['strPassword']);
			
			if ((strlen($strUsername)) && (strlen($strPassword))) {
				$strQuery					= sprintf(	"SELECT intID, intType, strName FROM %sapb_concierge_accounts WHERE strUsername = '%s' AND strPassword = '%s' LIMIT 1", 
														$this->db->prefix, 
														$this->db->escape($strUsername), 
														$this->db->escape($strPassword));
				$queryCheckAuth				= $this->db->get_results($strQuery);
				
				if (count($queryCheckAuth)) {
					$objRow						= $queryCheckAuth[0];
					
					$_SESSION['boolAPBC_Logon']	= true;
					$_SESSION['intAPBC_ID']		= intval($objRow->intID);
					$_SESSION['intAPBC_Type']	= intval($objRow->intType);
					$_SESSION['strAPBC_Name']	= stripslashes($objRow->strName);
	
					unset($queryCheckAuth);
					
					// TODO: allow configuration settings in the WP backend to "pick" the concierge calendar page
					header('Location: ' . get_bloginfo('url') . '/concierge/');
					die();
				}
				unset($queryCheckAuth);
			}
		}
	}
	
	public function logout() {
		if (isset($_SESSION['boolAPBC_Logon']))			unset($_SESSION['boolAPBC_Logon']);
		if (isset($_SESSION['intAPBC_ID']))				unset($_SESSION['intAPBC_ID']);
		if (isset($_SESSION['intAPBC_Type']))			unset($_SESSION['intAPBC_Type']);
		if (isset($_SESSION['strAPBC_Name']))			unset($_SESSION['strAPBC_Name']);
	}

	/*	
	==========================================================================
	ADMIN FUNCTIONS
	==========================================================================	
	*/
	public function admin_index() {
		$objAdmin			= new APB_Concierge_Admin_Accounts();
		$strActionType		= ((isset($_REQUEST['action'])) ? strtolower($_REQUEST['action']) : '');
		
		switch ($strActionType) {
			case 'commit' : 
				$objAdmin->commit();
				break;
				
			case 'insert' :
				$objAdmin->insert();
				break;
				
			case 'remove' : 
				$objAdmin->remove();
				break;
				
			case 'edit' : 
				$objAdmin->edit();
				break;
				
			case 'add' : 
				$objAdmin->add();
				break;
				
			default : 
				$objAdmin->index();
				break;
		}
	}

	public function admin_markets() {
		$objAdmin			= new APB_Concierge_Admin_Markets();
		$strActionType		= ((isset($_REQUEST['action'])) ? strtolower($_REQUEST['action']) : '');
		
		switch ($strActionType) {
			case 'commit' : 
				$objAdmin->commit();
				break;
				
			case 'insert' :
				$objAdmin->insert();
				break;
				
			case 'remove' : 
				$objAdmin->remove();
				break;
				
			case 'edit' : 
				$objAdmin->edit();
				break;
				
			case 'add' : 
				$objAdmin->add();
				break;
				
			default : 
				$objAdmin->index();
				break;
		}
	}

	public function admin_procedure_types() {
		$objAdmin			= new APB_Concierge_Admin_ProcedureTypes();
		$strActionType		= ((isset($_REQUEST['action'])) ? strtolower($_REQUEST['action']) : '');
		
		switch ($strActionType) {
			case 'commit' : 
				$objAdmin->commit();
				break;
				
			case 'insert' :
				$objAdmin->insert();
				break;
				
			case 'remove' : 
				$objAdmin->remove();
				break;
				
			case 'edit' : 
				$objAdmin->edit();
				break;
				
			case 'add' : 
				$objAdmin->add();
				break;
				
			default : 
				$objAdmin->index();
				break;
		}
	}

	public function admin_procedures() {
		$objAdmin			= new APB_Concierge_Admin_Procedures();
		$strActionType		= ((isset($_REQUEST['action'])) ? strtolower($_REQUEST['action']) : '');
		
		switch ($strActionType) {
			case 'commit' : 
				$objAdmin->commit();
				break;
				
			case 'insert' :
				$objAdmin->insert();
				break;
				
			case 'remove' : 
				$objAdmin->remove();
				break;
				
			case 'edit' : 
				$objAdmin->edit();
				break;
				
			case 'add' : 
				$objAdmin->add();
				break;
				
			default : 
				$objAdmin->index();
				break;
		}
	}

	public function admin_procedures_price_overrides() {
		$objAdmin			= new APB_Concierge_Admin_Procedures_Price_Overrides();
		$strActionType		= ((isset($_REQUEST['action'])) ? strtolower($_REQUEST['action']) : '');
		
		switch ($strActionType) {
			case 'commit' : 
				$objAdmin->commit();
				break;
				
			case 'insert' :
				$objAdmin->insert();
				break;
				
			case 'remove' : 
				$objAdmin->remove();
				break;
				
			case 'edit' : 
				$objAdmin->edit();
				break;
				
			case 'add' : 
				$objAdmin->add();
				break;
				
			default : 
				$objAdmin->index();
				break;
		}
	}

	public function admin_doctors() {
		$objAdmin			= new APB_Concierge_Admin_Doctors();
		$strActionType		= ((isset($_REQUEST['action'])) ? strtolower($_REQUEST['action']) : '');
		
		switch ($strActionType) {
			case 'commit' : 
				$objAdmin->commit();
				break;
				
			case 'insert' :
				$objAdmin->insert();
				break;
				
			case 'remove' : 
				$objAdmin->remove();
				break;
				
			case 'edit' : 
				$objAdmin->edit();
				break;
				
			case 'add' : 
				$objAdmin->add();
				break;
				
			default : 
				$objAdmin->index();
				break;
		}
	}

	public function admin_financing_terms() {
		$strActionType		= ((isset($_REQUEST['action'])) ? strtolower($_REQUEST['action']) : '');
		
		switch ($strActionType) {
			default : 
				include '_views/admin-financing-list.phtml';
				break;
		}
	}

	/*	
	==========================================================================
	FRONT-END AND DATA ACCESS FUNCTIONS
	==========================================================================	
	*/
	public function fetch_appointments() {
		$arrResults				= array();
		
		if ((self::is_logged_in()) && 
			(isset($_POST['dtStart'])) && 
			(isset($_POST['dtEnd']))) {
			$strAdditionalQuery		= "";
			
			if ((isset($_POST['intMarketID'])) && (intval($_POST['intMarketID']) > 0)) {
				$strAdditionalQuery		.=	sprintf(	"	AND a.intMarketID = %d ", 
														intval($_POST['intMarketID']));
			}
			if ((isset($_POST['arrConcierges'])) && (is_array($_POST['arrConcierges'])) && (count($_POST['arrConcierges']))) {
				$strAdditionalQuery		.=	sprintf(	"	AND a.intConciergeID IN (%s) ", 
														implode(',', $_POST['arrConcierges']));
			}
			
			$strQuery				= sprintf(	"	SELECT a.*, c.strName AS strConciergeName, d.strName AS strDoctorName, m.strName AS strMarketName, u.strName AS strCreatorName 
													FROM %sapb_concierge_appointments a 
													LEFT JOIN %sapb_concierge_accounts c ON a.intConciergeID = c.intID 
													LEFT JOIN %sapb_concierge_doctors d ON a.intDoctorID = d.intID 
													LEFT JOIN %sapb_concierge_markets m ON a.intMarketID = m.intID 
													LEFT JOIN %sapb_concierge_accounts u ON a.intCreatorID = u.intID 
													WHERE DATE(a.dtStartDate) >= '%s' 
													AND DATE(a.dtEndDate) <= '%s' 
													%s 
													ORDER BY a.dtStartDate ASC", 
												$this->db->prefix, 
												$this->db->prefix, 
												$this->db->prefix, 
												$this->db->prefix, 
												$this->db->prefix, 
												gmdate('Y-m-d', strtotime($_POST['dtStart'])), 
												gmdate('Y-m-d', strtotime($_POST['dtEnd'])), 
												$strAdditionalQuery);
			$queryGetAppointments	= $this->db->get_results($strQuery);
			
			if (count($queryGetAppointments)) {
				$strQuery				= sprintf(	"	SELECT a.intAppointmentID, a.intProcedureID, p.strTitle, p.txtDescription 
														FROM %sapb_concierge_appointments_procedures a 
														LEFT JOIN %sapb_concierge_procedures p ON a.intProcedureID = p.intID 
														WHERE a.intAppointmentID IN (
															SELECT a.intID 
															FROM %sapb_concierge_appointments a 
															WHERE DATE(a.dtStartDate) >= '%s' 
															AND DATE(a.dtEndDate) <= '%s' 
															%s
														)", 
													$this->db->prefix, 
													$this->db->prefix, 
													$this->db->prefix, 
													gmdate('Y-m-d', strtotime($_POST['dtStart'])), 
													gmdate('Y-m-d', strtotime($_POST['dtEnd'])), 
													$strAdditionalQuery);
				$queryGetProcedures		= $this->db->get_results($strQuery);
				
				foreach ($queryGetAppointments as $objRow) {
					$intCurID				= intval($objRow->intID);
					$strCurStartDate		= stripslashes($objRow->dtStartDate);
					$strCurEndDate			= stripslashes($objRow->dtEndDate);
					$dtCurStartDate			= strtotime($strCurStartDate);
					$dtCurEndDate			= strtotime($strCurEndDate);
					$intCurConciergeID		= intval($objRow->intConciergeID);
					$strCurConciergeName	= stripslashes($objRow->strConciergeName);
					$intCurDoctorID			= intval($objRow->intDoctorID);
					$strCurDoctorName		= stripslashes($objRow->strDoctorName);
					$intCurMarketID			= intval($objRow->intMarketID);
					$strCurMarketName		= stripslashes($objRow->strMarketName);
					$arrCurProcedures		= array();
					$arrCurProceduresTitles	= array();
					$strCurPatientFirstName	= stripslashes($objRow->strPatientFirstName);
					$strCurPatientLastName	= stripslashes($objRow->strPatientLastName);
					$strCurPatientEmail		= stripslashes($objRow->strPatientEmail);
					$strCurPatientPhone		= stripslashes($objRow->strPatientPhone);
					$txtCurNotes			= stripslashes($objRow->txtNotes);
					$strCurCreatedBy		= stripslashes($objRow->strCreatorName);
					$dtCurCreatedDate		= strtotime(stripslashes($objRow->dtCreated));
					
					if (count($queryGetProcedures)) {
						foreach ($queryGetProcedures as $objRow) {
							$intCurAppointmentID	= intval($objRow->intAppointmentID);
							
							if ($intCurAppointmentID == $intCurID) {
								$intCurProcedureID		= intval($objRow->intProcedureID);
								$strCurProcedureTitle	= stripslashes($objRow->strTitle);
								$txtCurProcedureDesc	= stripslashes($objRow->txtDescription);
								
								array_push(	$arrCurProceduresTitles, $strCurProcedureTitle);
								array_push(	$arrCurProcedures, 
											array(
												'id'		=> $intCurProcedureID, 
												'title'		=> $strCurProcedureTitle, 
												'desc'		=> $txtCurProcedureDesc
											));
							}
						}
					}
					
					// build tooltip
					$txtCurTooltip			= 	'<h1>' . $strCurPatientLastName . ', ' . $strCurPatientFirstName . '<a class="close" href="javascript:;">Close</a></h1>' . 
												'<small>With ' . $strCurConciergeName . ' on ' . date('F jS, Y', $dtCurStartDate) . ' from ' . date('g:i a', $dtCurStartDate) . ' to ' . date('g:i a', $dtCurEndDate) . '</small>' . 
												'<p>' . 
													implode(', ', $arrCurProceduresTitles) . 
													'<br />' . 
													'<small>Scheduled by ' . $strCurCreatedBy . ' on ' . date('F jS, Y', $dtCurCreatedDate) . '</small>' . 
												'</p>' . 
												'<ul>' . 
													'<li><a href="javascript:;" rel="' . $intCurID . '" class="edit">Edit</a></li>' . 
													'<li><a href="javascript:;" rel="' . $intCurID . '" class="email" data-firstname="' . $strCurPatientFirstName . '" data-lastname="' . $strCurPatientLastName . '" data-email="' . $strCurPatientEmail . '">Send E-mail</a></li>' . 
													'<li class="remove"><a href="javascript:;" rel="' . $intCurID . '" class="remove">Delete</a></li>' . 
												'</ul>';

					array_push(	$arrResults, 
								array(
									'id'			=> $intCurID, 
									'title'			=> $strCurPatientLastName . ', ' . $strCurPatientFirstName, 
									'allDay'		=> false, 
									'start'			=> $strCurStartDate, 
									'end'			=> $strCurEndDate, 
									'className'		=> 'market-' . $intCurMarketID . ' doctor-' . $intCurDoctorID, 
									'tooltip'		=> $txtCurTooltip
								));
				}
			}
			unset($queryGetAppointments);
		}
		
		header('Content-Type: application/json');
		print json_encode($arrResults);
		die();
	}
	
	public function fetch_appointment_details() {
		$arrResults				= array();
		
		if ((self::is_logged_in()) && 
			(isset($_POST['intAppointmentID'])) && 
			(intval($_POST['intAppointmentID']) > 0)) {
			$strQuery				= sprintf(	"	SELECT * 
													FROM %sapb_concierge_appointments 
													WHERE intID = %d 
													LIMIT 1", 
												$this->db->prefix, 
												intval($_POST['intAppointmentID']));
			$queryGetAppointment	= $this->db->get_results($strQuery);
			
			if (count($queryGetAppointment)) {
				$objRow					= $queryGetAppointment[0];
				$intCurID				= intval($objRow->intID);
				$strCurStartDate		= stripslashes($objRow->dtStartDate);
				$strCurEndDate			= stripslashes($objRow->dtEndDate);
				$dtCurStartDate			= strtotime($strCurStartDate);
				$dtCurEndDate			= strtotime($strCurEndDate);
				$intCurConciergeID		= intval($objRow->intConciergeID);
				$intCurDoctorID			= intval($objRow->intDoctorID);
				$intCurMarketID			= intval($objRow->intMarketID);
				$arrCurProcedureIDs		= array();
				$strCurPatientFirstName	= stripslashes($objRow->strPatientFirstName);
				$strCurPatientLastName	= stripslashes($objRow->strPatientLastName);
				$strCurPatientEmail		= stripslashes($objRow->strPatientEmail);
				$strCurPatientPhone		= stripslashes($objRow->strPatientPhone);
				$txtCurNotes			= stripslashes($objRow->txtNotes);

				$strQuery				= sprintf(	"	SELECT intProcedureID 
														FROM %sapb_concierge_appointments_procedures 
														WHERE intAppointmentID = %d", 
													$this->db->prefix, 
													intval($intCurID));
				$queryGetProcedures		= $this->db->get_results($strQuery);
				
				if (count($queryGetProcedures)) {
					foreach ($queryGetProcedures as $objRow) {
						$intCurProcedureID		= intval($objRow->intProcedureID);
						
						array_push($arrCurProcedureIDs, $intCurProcedureID);
					}
				}
				
				$arrResults				= array(	'id'			=> $intCurID, 
													'startDate'		=> date('Y-m-d', $dtCurStartDate), 
													'startTime'		=> date('h:i A', $dtCurStartDate), 
													'endDate'		=> date('Y-m-d', $dtCurEndDate), 
													'endTime'		=> date('h:i A', $dtCurEndDate), 
													'startFull'		=> date('m/d/Y H:i:s', $dtCurStartDate), 
													'endFull'		=> date('m/d/Y H:i:s', $dtCurEndDate), 
													'concierge'		=> $intCurConciergeID, 
													'doctor'		=> $intCurDoctorID, 
													'market'		=> $intCurMarketID, 
													'procedures'	=> $arrCurProcedureIDs, 
													'firstName'		=> $strCurPatientFirstName, 
													'lastName'		=> $strCurPatientLastName, 
													'email'			=> $strCurPatientEmail, 
													'phone'			=> $strCurPatientPhone, 
													'notes'			=> $txtCurNotes);
			}
			unset($queryGetAppointment);
		}
		
		header('Content-Type: application/json');
		print json_encode($arrResults);
		die();
	}

	public function create_appointment() {
		$arrResults				= array('success' => true);
		
		if ((self::is_logged_in()) && 
			(isset($_POST['strDateStart'])) && 
			(isset($_POST['strTimeStart'])) && 
			(isset($_POST['strDateEnd'])) && 
			(isset($_POST['strTimeEnd'])) && 
			(isset($_POST['intConciergeID'])) && 
			(isset($_POST['intMarketID'])) && 
			(isset($_POST['intDoctorID'])) && 
			(isset($_POST['strFirstName'])) && 
			(isset($_POST['strLastName'])) && 
			(isset($_POST['strEmail'])) && 
			(isset($_POST['strPhone'])) && 
			(isset($_POST['txtNotes'])) && 
			(strlen($_POST['strDateStart'])) && 
			(strlen($_POST['strTimeStart'])) && 
			(strlen($_POST['strDateEnd'])) && 
			(strlen($_POST['strTimeEnd'])) && 
			(intval($_POST['intConciergeID']) > 0) && 
			(intval($_POST['intMarketID']) > 0) && 
			(strlen($_POST['strFirstName'])) && 
			(strlen($_POST['strLastName']))) {
			$dtNewStartDate				= strtotime(stripslashes($_POST['strDateStart'] . ' ' . stripslashes($_POST['strTimeStart'])));
			$dtNewEndDate				= strtotime(stripslashes($_POST['strDateEnd'] . ' ' . stripslashes($_POST['strTimeEnd'])));
			$intNewConciergeID			= intval($_POST['intConciergeID']);
			$intNewMarketID				= intval($_POST['intMarketID']);
			$intNewDoctorID				= intval($_POST['intDoctorID']);
			$arrNewProcedureIDs			= $_POST['arrProcedureIDs'];
			$strNewFirstName			= strip_tags(stripslashes($_POST['strFirstName']));
			$strNewLastName				= strip_tags(stripslashes($_POST['strLastName']));
			$strNewEmail				= strip_tags(stripslashes($_POST['strEmail']));
			$strNewPhone				= strip_tags(stripslashes($_POST['strPhone']));
			$txtNewNotes				= strip_tags(stripslashes($_POST['txtNotes']));

			// TODO: implement check to ensure the logged-in user has access to the market that they're adding the appointment to

			$strQuery					= sprintf(	"	INSERT INTO %sapb_concierge_appointments (dtStartDate, dtEndDate, intConciergeID, intMarketID, intDoctorID, strPatientFirstName, strPatientLastName, strPatientEmail, strPatientPhone, txtNotes, intCreatorID, dtCreated) 
														VALUES ('%s', '%s', %d, %d, %d, '%s', '%s', '%s', '%s', '%s', %d, NOW())", 
													$this->db->prefix, 
													gmdate("Y-m-d H:i:s", $dtNewStartDate), 
													gmdate("Y-m-d H:i:s", $dtNewEndDate), 
													intval($intNewConciergeID), 
													intval($intNewMarketID), 
													intval($intNewDoctorID), 
													$this->db->escape($strNewFirstName), 
													$this->db->escape($strNewLastName), 
													$this->db->escape($strNewEmail), 
													$this->db->escape($strNewPhone), 
													$this->db->escape($txtNewNotes), 
													intval($_SESSION['intAPBC_ID']));
			$this->db->query($strQuery);
			
			// add procedures
			$intNewAppointmentID		= intval($this->db->insert_id);

			if ($intNewAppointmentID > 0) {
				if (isset($_POST['arrProcedureIDs'])) {
					foreach ($arrNewProcedureIDs as $intNewProcedureID) {
						$strQuery						= sprintf(	"INSERT INTO %sapb_concierge_appointments_procedures (intAppointmentID, intProcedureID) VALUES (%d, %d)", 
																	$this->db->prefix, 
																	intval($intNewAppointmentID), 
																	intval($intNewProcedureID));
						$this->db->query($strQuery);
					}
				}
			}
		} else {
			$arrResults['success']		= false;
		}
			
		header('Content-Type: application/json');
		print json_encode($arrResults);
		die();
	}
	
	public function update_appointment() {
		$arrResults				= array('success' => true);
		
		if ((self::is_logged_in()) && 
			(isset($_POST['intAppointmentID'])) && 
			(isset($_POST['strDateStart'])) && 
			(isset($_POST['strTimeStart'])) && 
			(isset($_POST['strDateEnd'])) && 
			(isset($_POST['strTimeEnd'])) && 
			(isset($_POST['intConciergeID'])) && 
			(isset($_POST['intMarketID'])) && 
			(isset($_POST['intDoctorID'])) && 
			(isset($_POST['strFirstName'])) && 
			(isset($_POST['strLastName'])) && 
			(isset($_POST['strEmail'])) && 
			(isset($_POST['strPhone'])) && 
			(isset($_POST['txtNotes'])) && 
			(intval($_POST['intAppointmentID']) > 0) && 
			(strlen($_POST['strDateStart'])) && 
			(strlen($_POST['strTimeStart'])) && 
			(strlen($_POST['strDateEnd'])) && 
			(strlen($_POST['strTimeEnd'])) && 
			(intval($_POST['intConciergeID']) > 0) && 
			(intval($_POST['intMarketID']) > 0) && 
			(strlen($_POST['strFirstName'])) && 
			(strlen($_POST['strLastName']))) {
			$intCurAppointmentID		= intval($_POST['intAppointmentID']);
			$dtCurStartDate				= strtotime(stripslashes($_POST['strDateStart'] . ' ' . stripslashes($_POST['strTimeStart'])));
			$dtCurEndDate				= strtotime(stripslashes($_POST['strDateEnd'] . ' ' . stripslashes($_POST['strTimeEnd'])));
			$intCurConciergeID			= intval($_POST['intConciergeID']);
			$intCurMarketID				= intval($_POST['intMarketID']);
			$intCurDoctorID				= intval($_POST['intDoctorID']);
			$arrCurProcedureIDs			= $_POST['arrProcedureIDs'];
			$strCurFirstName			= strip_tags(stripslashes($_POST['strFirstName']));
			$strCurLastName				= strip_tags(stripslashes($_POST['strLastName']));
			$strCurEmail				= strip_tags(stripslashes($_POST['strEmail']));
			$strCurPhone				= strip_tags(stripslashes($_POST['strPhone']));
			$txtCurNotes				= strip_tags(stripslashes($_POST['txtNotes']));

			// TODO: implement check to ensure the logged-in user has access to the market that they're adding the appointment to
			
			$strQuery					= sprintf(	"	UPDATE 	%sapb_concierge_appointments 
														SET		dtStartDate				= '%s', 
																dtEndDate				= '%s', 
																intConciergeID			= %d, 
																intMarketID				= %d, 
																intDoctorID				= %d, 
																strPatientFirstName		= '%s', 
																strPatientLastName		= '%s', 
																strPatientEmail			= '%s', 
																strPatientPhone			= '%s', 
																txtNotes				= '%s'
														WHERE	intID					= %d", 
													$this->db->prefix, 
													gmdate("Y-m-d H:i:s", $dtCurStartDate), 
													gmdate("Y-m-d H:i:s", $dtCurEndDate), 
													intval($intCurConciergeID), 
													intval($intCurMarketID), 
													intval($intCurDoctorID), 
													$this->db->escape($strCurFirstName), 
													$this->db->escape($strCurLastName), 
													$this->db->escape($strCurEmail), 
													$this->db->escape($strCurPhone), 
													$this->db->escape($txtCurNotes), 
													intval($intCurAppointmentID));
			$this->db->query($strQuery);
			
			// add procedures
			$strQuery					= sprintf(	"DELETE FROM %sapb_concierge_appointments_procedures WHERE intAppointmentID = %d", 
													$this->db->prefix, 
													intval($intCurAppointmentID));
			$this->db->query($strQuery);

			if (isset($_POST['arrProcedureIDs'])) {
				foreach ($arrCurProcedureIDs as $intCurProcedureID) {
					$strQuery						= sprintf(	"INSERT INTO %sapb_concierge_appointments_procedures (intAppointmentID, intProcedureID) VALUES (%d, %d)", 
																$this->db->prefix, 
																intval($intCurAppointmentID), 
																intval($intCurProcedureID));
					$this->db->query($strQuery);
				}
			}
		} else {
			$arrResults['success']		= false;
		}
			
		header('Content-Type: application/json');
		print json_encode($arrResults);
		die();
	}
		
	public function update_appointment_time() {
		$arrResults				= array('success' => true);
		
		if ((self::is_logged_in()) && 
			(isset($_POST['intAppointmentID'])) && 
			(isset($_POST['intDeltaDays'])) && 
			(isset($_POST['intDeltaMinutes'])) && 
			(intval($_POST['intAppointmentID']) > 0)) {
			$intCurAppointmentID		= intval($_POST['intAppointmentID']);
			$intCurAppointmentDeltaDays	= intval($_POST['intDeltaDays']);
			$intCurAppointmentDeltaMins	= intval($_POST['intDeltaMinutes']);
			
			// TODO: implement check to ensure the appointment they're editing is in a market they have access to
			
			if (isset($_POST['boolAdjustEndDateOnly'])) {
				$strQuery					= sprintf(	"	UPDATE	%sapb_concierge_appointments 
															SET 	dtEndDate = DATE_ADD(DATE_ADD(dtEndDate, INTERVAL %d MINUTE), INTERVAL %d DAY) 
															WHERE	intID = %d 
															LIMIT	1", 
															$this->db->prefix, 
															intval($intCurAppointmentDeltaMins), 
															intval($intCurAppointmentDeltaDays), 
															intval($intCurAppointmentID));
			} else {
				$strQuery					= sprintf(	"	UPDATE	%sapb_concierge_appointments 
															SET 	dtStartDate = DATE_ADD(DATE_ADD(dtStartDate, INTERVAL %d MINUTE), INTERVAL %d DAY), 
																	dtEndDate = DATE_ADD(DATE_ADD(dtEndDate, INTERVAL %d MINUTE), INTERVAL %d DAY) 
															WHERE	intID = %d 
															LIMIT	1", 
															$this->db->prefix, 
															intval($intCurAppointmentDeltaMins), 
															intval($intCurAppointmentDeltaDays), 
															intval($intCurAppointmentDeltaMins), 
															intval($intCurAppointmentDeltaDays), 
															intval($intCurAppointmentID));
			}
			$this->db->query($strQuery);
		} else {
			$arrResults['success']		= false;
		}

		header('Content-Type: application/json');
		print json_encode($arrResults);
		die();
	}

	public function remove_appointment() {
		$arrResults				= array('success' => true);
		
		if ((self::is_logged_in()) && 
			(isset($_POST['intAppointmentID'])) && 
			(intval($_POST['intAppointmentID']) > 0)) {
			$intCurAppointmentID		= intval($_POST['intAppointmentID']);

			// TODO: implement check to ensure the logged-in user has access to the market that they're adding the appointment to
			
			$strQuery					= sprintf(	"DELETE FROM %sapb_concierge_appointments_procedures WHERE intAppointmentID = %d", 
													$this->db->prefix, 
													intval($intCurAppointmentID));
			$this->db->query($strQuery);

			$strQuery					= sprintf(	"DELETE FROM %sapb_concierge_appointments WHERE intID = %d LIMIT 1", 
													$this->db->prefix, 
													intval($intCurAppointmentID));
			$this->db->query($strQuery);
		} else {
			$arrResults['success']		= false;
		}
			
		header('Content-Type: application/json');
		print json_encode($arrResults);
		die();
	}

	public function send_email_reminder() {
		$arrResults				= array('success' => true);
		
		if ((self::is_logged_in()) && 
			(isset($_POST['intAppointmentID'])) && 
			(intval($_POST['intAppointmentID']) > 0)) {
			$intCurAppointmentID		= intval($_POST['intAppointmentID']);

			// TODO: implement check to ensure the logged-in user has access to the market that they're adding the appointment to
			
			$strQuery					= sprintf(	"	SELECT a.dtStartDate, a.dtEndDate, a.strPatientFirstName, a.strPatientLastName, a.strPatientEmail, c.strName AS strConciergeName 
														FROM %sapb_concierge_appointments a 
														LEFT JOIN %sapb_concierge_accounts c ON a.intConciergeID = c.intID 
														WHERE a.intID = %d 
														LIMIT 1", 
													$this->db->prefix, 
													$this->db->prefix, 
													intval($intCurAppointmentID));
			$queryGetAppointmentDetails	= $this->db->get_results($strQuery);
			
			if (count($queryGetAppointmentDetails)) {
				$objRow						= $queryGetAppointmentDetails[0];
				
				$dtCurStartDate				= strtotime($objRow->dtStartDate);
				$dtCurEndDate				= strtotime($objRow->dtEndDate);
				$strCurAppointmentStartDate	= date('F, jS Y', $dtCurStartDate);
				$strCurAppointmentStartTime	= date('h:i A', $dtCurStartDate);
				$strCurAppointmentEndDate	= date('F, jS Y', $dtCurEndDate);
				$strCurAppointmentEndTime	= date('h:i A', $dtCurEndDate);
				$strCurPatientFirstName		= ucfirst(strtolower(stripslashes($objRow->strPatientFirstName)));
				$strCurPatientLastName		= stripslashes($objRow->strPatientLastName);
				$strCurPatientEmail			= stripslashes($objRow->strPatientEmail);
				$strCurConciergeName		= stripslashes($objRow->strConciergeName);

				if (strlen($strCurPatientEmail)) {
					$strPath					= plugin_dir_path(__FILE__);
					
					$objHandle					= fopen($strPath . '_templates/reminder.html', 'r');
					
					if ($objHandle) {
						$txtTemplateHTML			= fread($objHandle, filesize($strPath . '_templates/reminder.html'));
						fclose($objHandle);
						
						$txtTemplateHTML			= str_replace('*|FNAME|*', $strCurPatientFirstName, $txtTemplateHTML);
						$txtTemplateHTML			= str_replace('*|LNAME|*', $strCurPatientLastName, $txtTemplateHTML);
						$txtTemplateHTML			= str_replace('*|EMAIL|*', $strCurPatientEmail, $txtTemplateHTML);
						$txtTemplateHTML			= str_replace('*|CONCIERGE|*', $strCurConciergeName, $txtTemplateHTML);
						$txtTemplateHTML			= str_replace('*|APPT_DATE|*', $strCurAppointmentStartDate, $txtTemplateHTML);
						$txtTemplateHTML			= str_replace('*|APPT_TIME|*', $strCurAppointmentStartTime, $txtTemplateHTML);
						
						$objTransport				= Swift_MailTransport::newInstance();
						$objMailer					= Swift_Mailer::newInstance($objTransport);
						$objMessage					= Swift_Message::newInstance('A reminder from A Perfect Body about your upcoming appointment on ' . $strCurAppointmentStartDate . '.');
						$objMessage->setFrom(array('info@aperfectbody.com' => 'A Perfect Body'));
						$objMessage->setTo(array($strCurPatientEmail));
						$objMessage->setBody(strip_tags($txtTemplateHTML));
						$objMessage->addPart($txtTemplateHTML, 'text/html');
						
						$objMailer->send($objMessage);
					} else {
						$arrResults['success']		= false;
						$arrResults['reason']		= $strPath;
					}
				} else {
					$arrResults['success']		= false;
					$arrResults['reason']		= 3;
				}
			} else {
				$arrResults['success']		= false;
				$arrResults['reason']		= 2;
			}
			unset($queryGetAppointmentDetails);
		} else {
			$arrResults['success']		= false;
			$arrResults['reason']		= 1;
		}
			
		header('Content-Type: application/json');
		print json_encode($arrResults);
		die();
	}

	/*	
	==========================================================================
	STATIC METHODS
	==========================================================================	
	*/
	public static function is_logged_in() {
		if ((isset($_SESSION['boolAPBC_Logon'])) && 
			(isset($_SESSION['intAPBC_ID'])) && 
			(isset($_SESSION['intAPBC_Type'])) && 
			(isset($_SESSION['strAPBC_Name'])) && 
			($_SESSION['boolAPBC_Logon'] === true) && 
			(intval($_SESSION['intAPBC_ID']) > 0) && 
			(intval($_SESSION['intAPBC_Type']) > 0) && 
			(strlen($_SESSION['strAPBC_Name']) > 0)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static function get_doctors() {
		global $wpdb;
		
		$strQuery				= sprintf(	"SELECT intID, strName FROM %sapb_concierge_doctors ORDER BY strName ASC", 
											$wpdb->prefix);
		$queryGetData			= $wpdb->get_results($strQuery);
		
		return $queryGetData;
	}

	public static function get_procedures_types() {
		global $wpdb;
		
		$strQuery				= sprintf(	"SELECT * FROM %sapb_concierge_procedures_types ORDER BY strName ASC", 
											$wpdb->prefix);
		$queryGetData			= $wpdb->get_results($strQuery);
		
		return $queryGetData;
	}

	public static function get_procedures() {
		global $wpdb;
		
		$strQuery				= sprintf(	"SELECT * FROM %sapb_concierge_procedures ORDER BY intType ASC, strTitle ASC", 
											$wpdb->prefix);
		$queryGetData			= $wpdb->get_results($strQuery);
		
		return $queryGetData;
	}

	public static function get_procedures_price_overrides() {
		global $wpdb;
		
		$strQuery				= sprintf(	"SELECT * FROM %sapb_concierge_procedures_price_overrides", 
											$wpdb->prefix);
		$queryGetData			= $wpdb->get_results($strQuery);
		
		return $queryGetData;
	}

	public static function get_markets($intLoginID, $intLoginType) {
		global $wpdb;
		
		if ($intLoginID > 0) {
			if (intval($intLoginType) == 1) {
				$strQuery			= sprintf(	"	SELECT intID, strName 
													FROM %sapb_concierge_markets 
													ORDER BY strName ASC", 
												$wpdb->prefix);
			} else {
				$strQuery			= sprintf(	"	SELECT intID, strName 
													FROM %sapb_concierge_markets 
													WHERE intID IN (
														SELECT intAccountID FROM %sapb_concierge_accounts_markets WHERE intMarketID IN (
															SELECT intMarketID FROM %sapb_concierge_accounts_markets WHERE intAccountID = %d
														)
													) 
													ORDER BY strName ASC", 
												$wpdb->prefix, 
												$wpdb->prefix, 
												$wpdb->prefix, 
												intval($intLoginID));
			}
			$queryGetData			= $wpdb->get_results($strQuery);

			return $queryGetData;
		} else {
			return false;
		}
	}

	public static function get_concierges($intLoginID, $intLoginType) {
		global $wpdb;
		
		// TODO: show only the concierges in markets that the logged in user has access to
		if ($intLoginID > 0) {
			if (intval($intLoginType) == 1) {
				$strQuery				= sprintf(	"	SELECT intID, intType, strName 
														FROM %sapb_concierge_accounts 
														ORDER BY strName ASC", 
													$wpdb->prefix);
			} else {
				$strQuery				= sprintf(	"	SELECT intID, intType, strName 
														FROM %sapb_concierge_accounts 
														WHERE intID IN (
															SELECT intAccountID FROM %sapb_concierge_accounts_markets WHERE intMarketID IN (
																SELECT intMarketID FROM %sapb_concierge_accounts_markets WHERE intAccountID = %d
															)
														)
														ORDER BY strName ASC", 
													$wpdb->prefix, 
													$wpdb->prefix, 
													$wpdb->prefix, 
													intval($intLoginID));
			}
			$queryGetData			= $wpdb->get_results($strQuery);
			
			return $queryGetData;
		} else {
			return false;
		}
	}

	public static function get_concierges_markets($intLoginID, $intLoginType) {
		global $wpdb;
		
		// TODO: show only the concierges in markets that the logged in user has access to
		if ($intLoginID > 0) {
			if (intval($intLoginType) == 1) {
				$strQuery				= sprintf(	"	SELECT a.intMarketID, a.intAccountID, m.strName 
														FROM %sapb_concierge_accounts_markets a 
														LEFT JOIN %sapb_concierge_markets m ON a.intMarketID = m.intID 
														ORDER BY m.strName ASC", 
													$wpdb->prefix, 
													$wpdb->prefix);
			} else {
				$strQuery				= sprintf(	"	SELECT a.intMarketID, a.intAccountID, m.strName 
														FROM %sapb_concierge_accounts_markets a 
														LEFT JOIN %sapb_concierge_markets m ON a.intMarketID = m.intID 
														WHERE a.intMarketID IN (
															SELECT intMarketID FROM %sapb_concierge_accounts_markets WHERE intAccountID = %d
														) 
														ORDER BY m.strName ASC", 
													$wpdb->prefix, 
													$wpdb->prefix, 
													$wpdb->prefix, 
													intval($intLoginID));
			}
			$queryGetData			= $wpdb->get_results($strQuery);
			
			return $queryGetData;
		} else {
			return false;
		}
	}

	public static function get_userdata($strDataType) {
		$strReturnValue				= '';
		
		if (self::is_logged_in()) {
			switch (strtolower($strDataType)) {
				case 'id' : 
					$strReturnValue			= $_SESSION['intAPBC_ID'];
					break;

				case 'name' : 
					$strReturnValue			= $_SESSION['strAPBC_Name'];
					break;

				case 'type' : 
					$strReturnValue			= $_SESSION['intAPBC_Type'];
					break;
			}
		}
		
		return $strReturnValue;
	}
}

global $objConcierge;
$objConcierge		= new APB_Concierge();
?>