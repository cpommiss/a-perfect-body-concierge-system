var framework = {
	p: {
		paths: {
			ajax: '', // admin_url('admin-ajax.php');
			appointments: '?action=apb_concierge_appointments_fetch', 
			edit: '?action=apb_concierge_fetch_appointment_details', 
			create: '?action=apb_concierge_create_appointment', 
			update: '?action=apb_concierge_update_appointment', 
			update_time: '?action=apb_concierge_update_appointment_time', 
			remove: '?action=apb_concierge_remove_appointment', 
			email: '?action=apb_concierge_send_email_reminder', 
			logout: '?action=apb_concierge_logout'
		}, 
		calendar: {
			object: null, 
			fetching: false, 
			saving: false, 
			tooltip: {
				pointer: null
			}, 
			concierges: [], 
			modal: {
				type: '', 
				meta: ''
			}
		}, 
		calculator: {
			data: {
				procedure_total: 0, 
				interest: 0, 
				interest_rate: 0, 
				loan_length: 0, 
				downpayment: 0, 
				principal: 0, 
				total: 0, 
				payment: 0, 
				payoff: 0
			}, 
			procedures: [], 
			interest_rates: [13.99, 17.99, 22.99, 29.99], 
			chart_built: false, 
			chart_data: []
		}
	}, 
	fn: {
		admin: {
			setup: function() {
				jQuery('#nav-user-logout, #user-logout').bind('click', framework.fn.admin.logout_modal);
				jQuery('#btnLogout').bind('click', framework.fn.admin.logout);
			}, 
			
			logout_modal: function(objEvent) {
				objEvent.preventDefault();
				
				jQuery('#modalLogout').modal('show');
			}, 
			
			logout: function() {
				jQuery('#modalLogoutProgress').modal('show');
				
				$.post(	framework.p.paths.ajax + framework.p.paths.logout, 
						{}, 
						function(objData) {
							window.location.reload();
						}, 
						'json');
			}
		}, 
		
		calendar: {
			setup: function() {
				framework.fn.calendar.concierges.refresh_list();
				
				framework.p.calendar.object			= 	jQuery('#calendar-holder').fullCalendar({
															header: {
																left: 'prev,next',
																center: 'title',
																right: 'month,agendaWeek,agendaDay'
															},
															loading: function(boolLoading, objView) {
																framework.p.calendar.fetching			= boolLoading;
																// framework.fn.calendar.show_loading_overlay(boolLoading);
															}, 
															eventSources: [
																{
																	events: framework.fn.calendar.events.fetch
																}
															], 
															eventDrop: framework.fn.calendar.events.dragndrop, 
															eventResize: framework.fn.calendar.events.resize, 
															eventColor: '#7b237a', 
															eventRender: framework.fn.calendar.events.render, 
															selectable: true,
															selectHelper: true,
															select: framework.fn.calendar.events.add,
															editable: true
														});
				
				// bind tooltip events
				jQuery('.qtip-content>h1>a.close').live('click', framework.fn.calendar.tooltip.hide);
				jQuery('.qtip-content>ul>li>a.edit').live('click', function(objEvent) { objEvent.preventDefault(); framework.fn.calendar.events.edit(parseInt(jQuery(this).attr('rel'))); });				
				jQuery('.qtip-content>ul>li>a.email').live('click', framework.fn.calendar.events.email);
				jQuery('.qtip-content>ul>li>a.remove').live('click', framework.fn.calendar.events.remove);

				// bind timepicker events on new appointment modal
				jQuery('#modalAppointment #dtNewTimeStart, #modalAppointment #dtNewTimeEnd').timepicker();
				
				// bind modal button events
				jQuery('#modalAppointment #intNewMarketID').live('change', function() { framework.fn.calendar.markets.update_modal_options(jQuery(this).val()); });
				jQuery('#modalAppointment #btnAppointmentSubmit').live('click', framework.fn.calendar.events.process);
				jQuery('#modalRemove #btnAppointmentRemove').live('click', framework.fn.calendar.events.remove_commit);
				jQuery('#modalEmailError #btnAppointmentEmailEdit').live('click', function() { jQuery('#modalEmailError').modal('hide'); framework.fn.calendar.events.edit(parseInt(framework.p.calendar.modal.meta)); });
				jQuery('#modalEmailConfirm #btnAppointmentEmailSend').live('click', framework.fn.calendar.events.email_send);
				jQuery('#modalEmailSent #btnEmailClose').live('click', function() { jQuery('#modalEmailSent').modal('hide'); });
				
				// build chosen dropdown on modal
				jQuery('#modalAppointment #arrNewProcedureIDs').chosen();
				
				// bind change events on doctor filter checkboxes
				jQuery('#filter-concierges input[type=checkbox]').bind('change', framework.fn.calendar.concierges.filter);

				// bind change events on market dropdowns
				jQuery('#filter-markets ul.dropdown-menu>li>a').bind('click', framework.fn.calendar.markets.update);
			}, 
			
			concierges: {
				refresh_list: function() {
					framework.p.calendar.concierges		= [];
					
					jQuery('#filter-concierges input[type=checkbox]:checked').each(function() {
						framework.p.calendar.concierges.push(jQuery(this).attr('value'));
					});
				}, 
				
				filter: function() {
					framework.fn.calendar.concierges.refresh_list();

					framework.p.calendar.object.fullCalendar('refetchEvents');
				}
			}, 
			
			markets: {
				update: function(objEvent) {
					objEvent.preventDefault();
					
					var intCurMarketID			= parseInt(jQuery(this).attr('rel'));
					
					if (!isNaN(intCurMarketID)) {
						jQuery('#filter-markets a.dropdown-toggle>span').html(jQuery(this).text());

						jQuery('#intMarketID').val(intCurMarketID);
						
						if (intCurMarketID > 0) {
							jQuery('#filter-concierges label').hide();
							jQuery('#filter-concierges label.market-' + intCurMarketID).show();
						} else {
							jQuery('#filter-concierges label').show();
						}

						framework.p.calendar.object.fullCalendar('refetchEvents');
					}
				}, 
				
				update_modal_options: function(intMarketID) {
					if (intMarketID > 0) {
						jQuery('#modalAppointment #intNewConciergeID option').css('display', 'none');
						jQuery('#modalAppointment #intNewConciergeID option.market-' + intMarketID).css('display', 'block');
					} else {
						jQuery('#modalAppointment #intNewConciergeID option').css('display', 'block');
					}
				}
			}, 
			
			events: {
				fetch: function(dtStart, dtEnd, fncCallback) {
					jQuery.post(	framework.p.paths.ajax + framework.p.paths.appointments, 
									{
										'dtStart': moment(dtStart).format('YYYY-MM-DD'), 
										'dtEnd': moment(dtEnd).format('YYYY-MM-DD'), 
										'intMarketID': jQuery('#intMarketID').val(), 
										'arrConcierges': framework.p.calendar.concierges
									}, 
									function(objData) {
										fncCallback(objData);
									}, 
									'json');
				}, 
				
				add: function(dtStart, dtEnd, boolAllDay) {
					framework.p.calendar.modal.type			= 'add';

					var objStartDate		= moment(dtStart);
					var objEndDate			= moment(dtEnd);
					
					var intStartMonth		= objStartDate.month();
					var intStartDay			= objStartDate.day();
					var intEndMonth			= objEndDate.month();
					var intEndDay			= objEndDate.day();
					var intStartHours		= dtStart.getHours();
					var intEndHours			= dtEnd.getHours();
					var strDateDisplay		= "";
					var intCurMarketID		= parseInt(jQuery('#intMarketID').val());
					
					if ((intStartMonth != intEndMonth) || (intStartDay != intEndDay)) {
						strDateDisplay			= objStartDate.format('MMMM Do') + '-' + objEndDate.format('Do, YYYY');
					} else {
						strDateDisplay			= objStartDate.format('MMMM Do YYYY');
					}
					
					if (intStartHours != 0) { 
						jQuery('#dtNewTimeStart').val(objStartDate.format('hh:mm A'));
					} else {
						jQuery('#dtNewTimeStart').val('');
					}
					if (intEndHours != 0) {
						jQuery('#dtNewTimeEnd').val(objEndDate.format('hh:mm A'));
					} else {
						jQuery('#dtNewTimeEnd').val('');
					}
					
					jQuery('#modalAppointmentLabel').html('New Appointment &mdash; ' + strDateDisplay);
					jQuery('#modalAppointment .control-group').removeClass('error');
					jQuery('#modalAppointment #intNewDoctorID, #modalAppointment #strNewFirstName, #modalAppointment #strNewLastName, #modalAppointment #strNewEmail, #modalAppointment #strNewPhone, #modalAppointment #txtNewNotes').val('');
					jQuery('#modalAppointment #arrNewProcedureIDs option').each(function() { jQuery(this).attr('selected', false); });

					// update chosen plugin
					jQuery('#modalAppointment #arrNewProcedureIDs').trigger("liszt:updated");

					// populate dates
					jQuery('#modalAppointment #dtNewDateStart').val(objStartDate.format('YYYY-MM-DD'));
					jQuery('#modalAppointment #dtNewDateEnd').val(objEndDate.format('YYYY-MM-DD'));

					// pre-select concierge based on the currently logged in user ID
					jQuery('#modalAppointment #intConciergeID').val(jQuery('#intConciergeID').val());
					
					// pre-select market based on the current calendar view
					jQuery('#modalAppointment #intNewMarketID').val(jQuery('#intMarketID').val());
					
					// hide concierges from the concierge drop-down that are not part of this market
					framework.fn.calendar.markets.update_modal_options(intCurMarketID);

					// update button text
					jQuery('#btnAppointmentSubmit').html('Create');
					
					// do it
					jQuery('#modalAppointment').modal('show');
					
					framework.p.calendar.object.fullCalendar('unselect');
				}, 
				
				edit: function(intAppointmentID) {
					if ((!isNaN(intAppointmentID)) && (intAppointmentID > 0)) {
						framework.p.calendar.modal.type			= 'edit';
						
						jQuery('#modalFetchProgress').modal('show');
						
						$.post( framework.p.paths.ajax + framework.p.paths.edit, 
								{
									'intAppointmentID':		intAppointmentID
								}, 
								function(objData) {
									jQuery('#modalFetchProgress').modal('hide');
									
									var intStart			= Date.parse(objData.startFull);
									var intEnd				= Date.parse(objData.endFull);
									var dtStart				= new Date();
									var dtEnd				= new Date();

									dtStart.setTime(intStart);
									dtEnd.setTime(intEnd);

									var objStartDate		= moment(dtStart);
									var objEndDate			= moment(dtEnd);
									
									var intStartMonth		= objStartDate.month();
									var intStartDay			= objStartDate.day();
									var intEndMonth			= objEndDate.month();
									var intEndDay			= objEndDate.day();
									
									var intStartHours		= dtStart.getHours();
									var intEndHours			= dtEnd.getHours();
									var strDateDisplay		= "";
									
									if ((intStartMonth != intEndMonth) || (intStartDay != intEndDay)) {
										strDateDisplay			= objStartDate.format('MMMM Do') + '-' + objEndDate.format('Do, YYYY');
									} else {
										strDateDisplay			= objStartDate.format('MMMM Do YYYY');
									}
									
									if (intStartHours != 0) { 
										jQuery('#dtNewTimeStart').val(objStartDate.format('hh:mm A'));
									} else {
										jQuery('#dtNewTimeStart').val('');
									}
									if (intEndHours != 0) {
										jQuery('#dtNewTimeEnd').val(objEndDate.format('hh:mm A'));
									} else {
										jQuery('#dtNewTimeEnd').val('');
									}
									
									jQuery('#modalAppointmentLabel').html('Editing Appointment &mdash; ' + strDateDisplay);
									jQuery('#modalAppointment .control-group').removeClass('error');
									jQuery('#modalAppointment #intNewAppointmentID').val(intAppointmentID);
									jQuery('#modalAppointment #intNewConciergeID').val(objData.concierge);
									jQuery('#modalAppointment #intNewMarketID').val(objData.market);
									jQuery('#modalAppointment #intNewDoctorID').val(objData.doctor);
									jQuery('#modalAppointment #strNewFirstName').val(objData.firstName);
									jQuery('#modalAppointment #strNewLastName').val(objData.lastName);
									jQuery('#modalAppointment #strNewEmail').val(objData.email);
									jQuery('#modalAppointment #strNewPhone').val(objData.phone);
									jQuery('#modalAppointment #txtNewNotes').val(objData.notes);
									jQuery('#modalAppointment #arrNewProcedureIDs option').each(function() { jQuery(this).attr('selected', false); });
									
									// select procedures
									if ((objData.procedures) && (objData.procedures.length)) {
										for (var intCounter = 0; intCounter < objData.procedures.length; intCounter++) {
											jQuery('#modalAppointment #arrNewProcedureIDs option[value="' + objData.procedures[intCounter] + '"]').attr('selected', true);
										}
									}
									
									// update chosen plugin
									jQuery('#modalAppointment #arrNewProcedureIDs').trigger("liszt:updated");
									
									// hide concierges from the concierge drop-down that are not part of this market
									framework.fn.calendar.markets.update_modal_options(objData.market);
									
									// populate dates
									jQuery('#modalAppointment #dtNewDateStart').val(objStartDate.format('YYYY-MM-DD'));
									jQuery('#modalAppointment #dtNewDateEnd').val(objEndDate.format('YYYY-MM-DD'));

									// update button text
									jQuery('#btnAppointmentSubmit').html('Update');
									
									// do it
									jQuery('#modalAppointment').modal('show');
								}, 
								'json');
					}
				}, 
				
				process: function() {
					if (!framework.p.calendar.saving) {
						var objDateStart			= jQuery('#modalAppointment #dtNewDateStart');
						var objDateEnd				= jQuery('#modalAppointment #dtNewDateEnd');
						var objTimeStart			= jQuery('#modalAppointment #dtNewTimeStart');
						var objTimeEnd				= jQuery('#modalAppointment #dtNewTimeEnd');
						var objConciergeID			= jQuery('#modalAppointment #intNewConciergeID');
						var objMarketID				= jQuery('#modalAppointment #intNewMarketID');
						var objDoctorID				= jQuery('#modalAppointment #intNewDoctorID');
						var arrProcedureIDs			= new Array();
						var objFirstName			= jQuery('#modalAppointment #strNewFirstName');
						var objLastName				= jQuery('#modalAppointment #strNewLastName');
						var objEmail				= jQuery('#modalAppointment #strNewEmail');
						var objPhone				= jQuery('#modalAppointment #strNewPhone');
						var objNotes				= jQuery('#modalAppointment #txtNewNotes');
	
						jQuery('#modalAppointment .control-group').removeClass('error');
						jQuery('#modalAppointment .alert').addClass('hidden');
						
						if (jQuery(objTimeStart).val().length == 0)		jQuery(objTimeStart).parents('.control-group').addClass('error');
						if (jQuery(objTimeEnd).val().length == 0)		jQuery(objTimeEnd).parents('.control-group').addClass('error');
						if (jQuery(objConciergeID).val().length == 0)	jQuery(objConciergeID).parents('.control-group').addClass('error');
						if (jQuery(objMarketID).val().length == 0)		jQuery(objMarketID).parents('.control-group').addClass('error');
						if (jQuery(objFirstName).val().length == 0)		jQuery(objFirstName).parents('.control-group').addClass('error');
						if (jQuery(objLastName).val().length == 0)		jQuery(objLastName).parents('.control-group').addClass('error');
						
						// populate procedure ID array with selected options
						jQuery('#modalAppointment #arrNewProcedureIDs option:selected').each(function() { arrProcedureIDs.push(this.value); });
						
						if (jQuery('#modalAppointment .control-group.error').length) {
							jQuery('#modalAppointment .alert').html('<strong>Sorry, but some required fields are missing.</strong><br />They have been outlined in red. Please complete these fields and then try again.').removeClass('hidden');
						} else {
							framework.p.calendar.saving		= true;
							
							switch (framework.p.calendar.modal.type) {
								case "add" : 
									jQuery('#modalSaveProgress').modal('show');
									
									jQuery.post(	framework.p.paths.ajax + framework.p.paths.create, 
													{
														'strDateStart':			jQuery(objDateStart).val(), 
														'strTimeStart':			jQuery(objTimeStart).val(), 
														'strDateEnd':			jQuery(objDateEnd).val(),
														'strTimeEnd':			jQuery(objTimeEnd).val(), 
														'intConciergeID':		jQuery(objConciergeID).val(), 
														'intMarketID':			jQuery(objMarketID).val(), 
														'intDoctorID':			jQuery(objDoctorID).val(), 
														'arrProcedureIDs':		arrProcedureIDs, 
														'strFirstName':			jQuery(objFirstName).val(), 
														'strLastName':			jQuery(objLastName).val(), 
														'strEmail':				jQuery(objEmail).val(), 
														'strPhone':				jQuery(objPhone).val(), 
														'txtNotes':				jQuery(objNotes).val()
													}, 
													function(objData) {
														framework.p.calendar.saving			= false;
														
														jQuery('#modalSaveProgress').modal('hide');

														if (objData.success === true) {
															jQuery('#modalAppointment').modal('hide');
															
															// refresh events
															framework.p.calendar.object.fullCalendar('refetchEvents');
														} else {
															alert('An unknown error occurred while trying to add this appointment. Please try again.');
														}
													}, 
													'json');
									break;

								case "edit" : 
									var intAppointmentID				= parseInt(jQuery('#modalAppointment #intNewAppointmentID').val());
									
									if ((!isNaN(intAppointmentID)) && (intAppointmentID > 0)) {
										jQuery('#modalSaveProgress').modal('show');

										jQuery.post(	framework.p.paths.ajax + framework.p.paths.update, 
														{
															'intAppointmentID':		intAppointmentID, 
															'strDateStart':			jQuery(objDateStart).val(), 
															'strTimeStart':			jQuery(objTimeStart).val(), 
															'strDateEnd':			jQuery(objDateEnd).val(),
															'strTimeEnd':			jQuery(objTimeEnd).val(), 
															'intConciergeID':		jQuery(objConciergeID).val(), 
															'intMarketID':			jQuery(objMarketID).val(), 
															'intDoctorID':			jQuery(objDoctorID).val(), 
															'arrProcedureIDs':		arrProcedureIDs, 
															'strFirstName':			jQuery(objFirstName).val(), 
															'strLastName':			jQuery(objLastName).val(), 
															'strEmail':				jQuery(objEmail).val(), 
															'strPhone':				jQuery(objPhone).val(), 
															'txtNotes':				jQuery(objNotes).val()
														}, 
														function(objData) {
															framework.p.calendar.saving			= false;

															jQuery('#modalSaveProgress').modal('hide');
															
															if (objData.success === true) {
																jQuery('#modalAppointment').modal('hide');
																
																// refresh events
																framework.p.calendar.object.fullCalendar('refetchEvents');
															} else {
																alert('An unknown error occurred while trying to update this appointment. Please try again.');
															}
														}, 
														'json');
									} else {
										alert('An error occurred while trying to update this appointment. Please try again.');
									}
									break;
							}
							
							framework.p.calendar.modal.type			= '';
						}
					}
				}, 
				
				remove: function() {
					var intAppointmentID				= parseInt(jQuery(this).attr('rel'));
					
					if ((!isNaN(intAppointmentID)) && (intAppointmentID > 0)) {
						jQuery('#modalRemove #intRemoveAppointmentID').val(intAppointmentID);
						
						jQuery('#modalRemove').modal('show');
					} else {
						alert('An error occurred while trying to remove this appointment. Please try again.');
					}
				}, 
				
				remove_commit: function() {
					if (!framework.p.calendar.saving) {
						var intAppointmentID				= parseInt(jQuery('#modalRemove #intRemoveAppointmentID').val());
						
						if ((!isNaN(intAppointmentID)) && (intAppointmentID > 0)) {
							framework.p.calendar.saving		= true;
							
							jQuery('#modalRemoveProgress').modal('show');
							
							jQuery.post(	framework.p.paths.ajax + framework.p.paths.remove, 
											{
												'intAppointmentID': intAppointmentID
											}, 
											function(objData) {
												framework.p.calendar.saving			= false;
												
												if (objData.success === true) {
													jQuery('#modalRemoveProgress').modal('hide');
													jQuery('#modalRemove').modal('hide');
													
													// refresh events
													framework.p.calendar.object.fullCalendar('refetchEvents');
												} else {
													alert('An unknown error occurred while trying to remove this appointment. Please try again.');
												}
											}, 
											'json');
						} else {
							alert('An error occurred while trying to remove this appointment. Please try again.');
						}
					}
				}, 
				
				email: function(objEvent) {
					objEvent.preventDefault();
					
					var intAppointmentID	= parseInt(jQuery(this).attr('rel'));
					var strFirstName		= jQuery(this).attr('data-firstname');
					var strLastName			= jQuery(this).attr('data-lastname');
					var strEmail			= jQuery(this).attr('data-email');
					
					if ((!isNaN(intAppointmentID)) && (intAppointmentID > 0) && (strFirstName.length)) {
						framework.p.calendar.modal.meta			= intAppointmentID;
						
						if (strEmail.length) {
							framework.p.calendar.modal.type			= 'email';
							
							jQuery('#modalEmailConfirm #modalEmailConfirmFirstName').html(strFirstName);
							jQuery('#modalEmailConfirm').modal('show');
						} else {
							jQuery('#modalEmailError').modal('show');
						}
					}
				}, 
				
				email_send: function() {
					if ((!isNaN(framework.p.calendar.modal.meta)) && (framework.p.calendar.modal.meta > 0)) {
						jQuery('#modalEmailConfirm').modal('hide');
						jQuery('#modalEmailProgress').modal('show');
						
						jQuery.post(	framework.p.paths.ajax + framework.p.paths.email, 
										{
											'intAppointmentID': framework.p.calendar.modal.meta
										}, 
										function(objData) {
											if (objData.success === true) {
												jQuery('#modalEmailProgress').modal('hide');
												jQuery('#modalEmailSent').modal('show');
											} else {
												alert('An unknown error occurred while trying to send this e-mail reminder. Please try again.');
											}
										}, 
										'json');
					}
				}, 
				
				resize: function(objEvent, intDeltaDays, intDeltaMinutes, fncRevertCallback, objDOMEvent, objUI, objView) {
					// TODO: add confirmation prompt, maybe?

					jQuery.post(	framework.p.paths.ajax + framework.p.paths.update_time, 
									{
										'intAppointmentID': objEvent.id, 
										'intDeltaDays': intDeltaDays, 
										'intDeltaMinutes': intDeltaMinutes, 
										'boolAdjustEndDateOnly': true
									}, 
									function(objData) {
										// assume success
									}, 
									'json');
				}, 
				
				dragndrop: function(objEvent, intDeltaDays, intDeltaMinutes, boolAllDay, fncRevertCallback) {
					// TODO: add confirmation prompt, maybe?

					jQuery.post(	framework.p.paths.ajax + framework.p.paths.update_time, 
									{
										'intAppointmentID': objEvent.id, 
										'intDeltaDays': intDeltaDays, 
										'intDeltaMinutes': intDeltaMinutes
									}, 
									function(objData) {
										// assume success
									}, 
									'json');
				}, 
				
				render: function(objEvent, objElement) {
					jQuery(objElement).qtip({
						content: {
							text: objEvent.tooltip
						}, 
						position: {
							corner: {
								target: 'topMiddle',
								tooltip: 'bottomMiddle'
							}
						},
						show: {
							when: 'click', 
							solo: true
						}, 
						hide: 'unfocus', 
						style: {
							width: 300, 
							tip: {
								corner: 'bottomMiddle'
							}, 
							border: {
								width: 1,
								radius: 4
							}, 
							name: 'light'
						}, 
						api: {
							onShow: framework.fn.calendar.tooltip.save
						}
					});
				}
			}, 
			
			tooltip: {
				save: function() {
					framework.p.calendar.tooltip.pointer		= this.elements.target;
				}, 
				
				hide: function() {
					if (typeof(framework.p.calendar.tooltip.pointer) == 'object') {
						jQuery(framework.p.calendar.tooltip.pointer).qtip('hide');
						
						framework.p.calendar.tooltip.pointer		= null;
					}
				}
			}
		}, 
		
		calculator: {
			setup: function() {
				jQuery(window).resize(framework.fn.calculator.resize);

				// equalize the height of the charting container so it matches that of the settings container
				jQuery('#widget-charting .widget-content').css('height', jQuery('#widget-settings .widget-content').height());
				jQuery('#widget-charting .widget-content #charting-placeholder').css({'width': '100%', 'height': '100%'});

				// bind change events on market dropdowns
				jQuery('#filter-markets ul.dropdown-menu>li>a').bind('click', framework.fn.calculator.markets.update);
				jQuery('#intMarketID').bind('change', framework.fn.calculator.markets.update_header);
				
				// bind change event on procedure type
				jQuery('#intProcedureType').bind('change', framework.fn.calculator.data.procedure_type);

				// bind change event on procedure type
				jQuery('#intProcedureID, #intDoctorID').bind('change', framework.fn.calculator.data.procedure);

				// bind change event on down payment
				jQuery('#intCreditScore').bind('change', framework.fn.calculator.data.creditscore);

				// bind change event on down payment
				jQuery('#intLoanLength').bind('change', framework.fn.calculator.data.loanlength);

				// bind click event on down payment entry
				jQuery('#btnDownPaymentApply').bind('click', framework.fn.calculator.data.downpayment);
			}, 
			
			resize: function() {
				if (framework.p.calculator.chart_built) {
					jQuery('#charting-placeholder').text('');

					jQuery('#widget-charting .widget-content').css('height', jQuery('#widget-settings .widget-content').height());
					
					framework.fn.calculator.charting.build();
				}
			}, 
			
			data: {
				format_currency: function(strPrepend, dblValue) {
					var arrData		= dblValue.toFixed(2).split(".");
					arrData[0]		= arrData[0].split("").reverse().join("").replace(/(\d{3})(?=\d)/g, "$1,").split("").reverse().join("");
					
					return strPrepend + arrData.join(".");
				}, 
				
				doctor: function() {
					framework.fn.calculator.data.procedure();
				}, 
				
				procedure_type: function() {
					var intProcedureType	= parseInt(jQuery(this).val());
					
					jQuery('#intProcedureID').attr('disabled', false);

					jQuery('#intProcedureID').empty();
					jQuery('#intProcedureID').append('<option value="">-- Select --</option>');

					if ((!isNaN(intProcedureType)) && (intProcedureType > 0)) {
						for (var intCounter = 0; intCounter < framework.p.calculator.procedures.length; intCounter++) {
							if (framework.p.calculator.procedures[intCounter].type == intProcedureType) {
								jQuery('#intProcedureID').append('<option value="' + framework.p.calculator.procedures[intCounter].id + '">' + framework.p.calculator.procedures[intCounter].title + '</option>');
							}
						}
						jQuery('#intProcedureID').attr('disabled', false);
					} else {
						jQuery('#intProcedureID').attr('disabled', true);
						jQuery('#intCreditScore, #intLoanLength, #strDownPayment').attr('disabled', true);
					}
				}, 

				procedure: function() {
					var intProcedureID		= parseInt(jQuery('#intProcedureID').val());
					var intDoctorID			= parseInt(jQuery('#intDoctorID').val());
					var dblProcedureTotal	= 0;
					var intCounter, intOverrideCounter;
					
					if ((!isNaN(intProcedureID)) && (intProcedureID > 0)) {
						for (intCounter = 0; intCounter < framework.p.calculator.procedures.length; intCounter++) {
							if (framework.p.calculator.procedures[intCounter].id == intProcedureID) {
								framework.p.calculator.data.procedure_total		= framework.p.calculator.procedures[intCounter].total;
								
								if ((framework.p.calculator.procedures[intCounter].overrides.length) && (!isNaN(intDoctorID)) && (intDoctorID > 0)) {
									for (intOverrideCounter = 0; intOverrideCounter < framework.p.calculator.procedures[intCounter].overrides.length; intOverrideCounter++) {
										if (framework.p.calculator.procedures[intCounter].overrides[intOverrideCounter].doctor == intDoctorID) {
											if (framework.p.calculator.procedures[intCounter].overrides[intOverrideCounter].total > 0) {
												framework.p.calculator.data.procedure_total		= framework.p.calculator.procedures[intCounter].overrides[intOverrideCounter].total
											}
											break;
										}
									}
								} else {
									break;
								}
							}
						}
						
						jQuery('#intCreditScore, #intLoanLength, #strDownPayment').attr('disabled', false);
						
						framework.fn.calculator.data.calculate();
					} else {
						framework.p.calculator.data.procedure_total		= 0;

						jQuery('#intCreditScore, #intLoanLength, #strDownPayment').attr('disabled', true);
					}
				}, 
				
				creditscore: function() {
					var intCreditScore		= parseInt(jQuery(this).val());
					
					if ((!isNaN(intCreditScore)) && (intCreditScore > 0)) {
						framework.p.calculator.data.interest_rate	= framework.p.calculator.interest_rates[intCreditScore - 1];
					} else {
						framework.p.calculator.data.interest_rate	= 0;
					}
					
					framework.fn.calculator.data.calculate();
				}, 

				loanlength: function() {
					var intLoanLength		= parseInt(jQuery(this).val());
					
					if ((!isNaN(intLoanLength)) && (intLoanLength > 0)) {
						framework.p.calculator.data.loan_length		= intLoanLength;
					} else {
						framework.p.calculator.data.loan_length		= 1;	// just calculate it with no monthly payments since there seems to be an invalid loan length encountered
					}
					
					framework.fn.calculator.data.calculate();
				}, 
				
				downpayment: function() {
					var strDownPayment		= jQuery('#strDownPayment').val().replace(',', '');
					
					if (strDownPayment.length) {
						framework.p.calculator.data.downpayment		= parseFloat(strDownPayment);
					} else {
						framework.p.calculator.data.downpayment		= 0;
					}

					framework.fn.calculator.data.calculate();
				}, 
				
				calculate: function() {
					if ((framework.p.calculator.data.procedure_total > 0) && 
						(framework.p.calculator.data.interest_rate > 0)) {
						if (framework.p.calculator.data.loan_length == 0) {
							framework.p.calculator.data.loan_length		= parseInt(jQuery('#intLoanLength').val());
							
							if ((isNaN(framework.p.calculator.data.loan_length)) || (framework.p.calculator.data.loan_length == 0)) {
								framework.p.calculator.data.loan_length		= 1;
							}
						}
						
						if ((!isNaN(framework.p.calculator.data.downpayment)) && (framework.p.calculator.data.downpayment > 0)) {
							framework.p.calculator.data.principal		= (framework.p.calculator.data.procedure_total - framework.p.calculator.data.downpayment);
						} else {
							framework.p.calculator.data.principal		= framework.p.calculator.data.procedure_total;
						}
						
						framework.p.calculator.data.interest		= (framework.p.calculator.data.principal * (framework.p.calculator.data.interest_rate / 100));
						framework.p.calculator.data.total			= (framework.p.calculator.data.principal + framework.p.calculator.data.interest);
						framework.p.calculator.data.payment			= (framework.p.calculator.data.total / framework.p.calculator.data.loan_length);
						
						framework.p.calculator.chart_data			= [
							{label: 'Principal', data: framework.p.calculator.data.principal}, 
							{label: 'Interest', data: framework.p.calculator.data.interest}
						];
						
						if (jQuery('#financing-stats').hasClass('hidden')) {
							jQuery('#financing-stats')
								.css({'opacity': 0})
								.removeClass('hidden')
								.animate({'opacity': 1}, 500);
						}
						
						jQuery('#stats-total-financed').html(framework.fn.calculator.data.format_currency('$', framework.p.calculator.data.total));
						jQuery('#stats-interest-rate').html(framework.p.calculator.data.interest_rate + '%');
						jQuery('#stats-monthly-payment').html(framework.fn.calculator.data.format_currency('$', framework.p.calculator.data.payment));
						jQuery('#stats-payoff-amount').html('n/a');
						
						framework.fn.calculator.charting.build();
					}
				}
			}, 
			
			markets: {
				update: function(objEvent) {
					objEvent.preventDefault();

					var intCurMarketID			= parseInt(jQuery(this).attr('rel'));
					
					if (!isNaN(intCurMarketID)) {
						jQuery('#filter-markets a.dropdown-toggle>span').html(jQuery(this).text());
						jQuery('#intMarketID').val(intCurMarketID);
					}
				}, 
				
				update_header: function() {
					jQuery('#filter-markets a.dropdown-toggle>span').html(jQuery('option:selected', this).text());
				}
			}, 
			
			charting: {
				build: function() {
					framework.p.calculator.chart_built		= true;
					
					// build
					jQuery.plot(	jQuery('#charting-placeholder'), 
									framework.p.calculator.chart_data, 
									{
										series: {
											pie: {
												show: true,
												radius: 1,
												label: {
													show: true,
													radius: 1/2,
													formatter: framework.fn.calculator.charting.label_formatter,
													threshold: 0.1
												}
											}
										},
										legend: {
											show: false
										}
									});
				}, 
				
				label_formatter: function(strLabel, objSeries) {
					return "<div style='font-size: 14px; line-height: 14px; font-weight: bold; text-shadow: 0px 1px 2px #000; color: #fff;'><span style='font-size: 12px;'>" + strLabel + "</span><br/>" + framework.fn.calculator.data.format_currency('$', objSeries.data[0][1]) + "</div>";
				}
			}
		}
	}
};