jQuery(document).ready(function() {
	jQuery('table.concierge-table a.delete').bind('click', function(objEvent) {
		if (confirm('Are you sure you want to delete this ' + jQuery(this).attr('data-type') + '?\n\nPress [OK] to continue, or [CANCEL] to abort.')) {
			return true;
		} else {
			return false;
		}
	});
	jQuery('#intProcedureID.override').bind('change', function() {
		var intProcedureID		= parseInt(this.value);
		
		if ((!isNaN(intProcedureID)) && (intProcedureID > 0)) {
			var objSelectedOption	= jQuery(this).find('option:selected');
			
			if (objSelectedOption.length) {
				jQuery('#dblProcedureFeeAnesthesia').siblings('span').html('Default: $' + jQuery(objSelectedOption).attr('data-default-anesthesia'));
				jQuery('#dblProcedureFeeFacility').siblings('span').html('Default: $' + jQuery(objSelectedOption).attr('data-default-facility'));
				jQuery('#dblProcedureFeeSurgeon').siblings('span').html('Default: $' + jQuery(objSelectedOption).attr('data-default-surgeon'));
				jQuery('#dblProcedureFeeTotal').siblings('span').html('Default: $' + jQuery(objSelectedOption).attr('data-default-total'));
			}
		} else {
			jQuery('#dblProcedureFeeAnesthesia').siblings('span').html('');
			jQuery('#dblProcedureFeeFacility').siblings('span').html('');
			jQuery('#dblProcedureFeeSurgeon').siblings('span').html('');
			jQuery('#dblProcedureFeeTotal').siblings('span').html('');
		}
	});
	jQuery('#intProcedureID.override').trigger('change');
});