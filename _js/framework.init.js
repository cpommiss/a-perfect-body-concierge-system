jQuery(document).ready(function() {
	framework.fn.admin.setup();
	
	switch (true) {
		case jQuery('body').hasClass('page-template-page-concierge-calendar-php') : 
			framework.fn.calendar.setup();
			break;

		case jQuery('body').hasClass('page-template-page-concierge-calculator-php') : 
			framework.fn.calculator.setup();
			break;
	}
});