<?php

class APB_Concierge_Admin_Procedures {
	const APB_MODULE_SLUG		= 'concierge-procedure';
	const APB_MODULE_TITLE		= 'Procedures';

	private $db;
	
	function __construct() {
		global $wpdb;
		
		$this->db			= &$wpdb;
	}
	
	public function index() {
		$strQuery			= sprintf(	"SELECT p.*, t.strName AS strProcedureTypeTitle FROM %sapb_concierge_procedures p LEFT JOIN %sapb_concierge_procedures_types t ON p.intType = t.intID ORDER BY t.strName ASC, p.strTitle ASC", 
										$this->db->prefix, 
										$this->db->prefix);
		$queryGetProcedures	= $this->db->get_results($strQuery);

		include dirname(__FILE__) . '/../_views/admin-procedures-list.phtml';
	}

	public function edit() {
		// common datasets
		$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_procedures_types ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetTypes		= $this->db->get_results($strQuery);

		if ((isset($_REQUEST['pid'])) && (intval($_REQUEST['pid']) > 0)) {
			$intCurProcedureID	= intval($_REQUEST['pid']);
			
			$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_procedures WHERE intID = %d LIMIT 1", 
											$this->db->prefix, 
											intval($intCurProcedureID));
			$queryGetProcedure	= $this->db->get_results($strQuery);

			if (count($queryGetProcedure)) {
				$objRow					= $queryGetProcedure[0];
				$intCurProcedureID		= intval($objRow->intID);
				$intCurProcedureType	= intval($objRow->intType);
				$strCurProcedureTitle	= stripslashes($objRow->strTitle);
				$txtCurProcedureDesc	= stripslashes($objRow->txtDescription);
				$dblCurFeeAnesthesia	= floatval($objRow->dblFeeAnesthesia);
				$dblCurFeeFacility		= floatval($objRow->dblFeeFacility);
				$dblCurFeeSurgeon		= floatval($objRow->dblFeeSurgeon);
				$dblCurFeeTotal			= floatval($objRow->dblFeeTotal);
				
				include dirname(__FILE__) . '/../_views/admin-procedures-edit.phtml';
			} else {
				$this->index();
			}
		} else {
			$this->index();
		}
	}
	
	public function add() {
		// common datasets
		$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_procedures_types ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetTypes		= $this->db->get_results($strQuery);

		include dirname(__FILE__) . '/../_views/admin-procedures-add.phtml';
	}

	public function commit() {
		if ((isset($_POST['pid'])) && 
			(isset($_POST['intProcedureType'])) && 
			(isset($_POST['strProcedureTitle'])) && 
			(isset($_POST['txtProcedureDesc'])) && 
			(isset($_POST['dblProcedureFeeAnesthesia'])) && 
			(isset($_POST['dblProcedureFeeFacility'])) && 
			(isset($_POST['dblProcedureFeeSurgeon'])) && 
			(isset($_POST['dblProcedureFeeTotal'])) && 
			(intval($_POST['pid']) > 0) && 
			(intval($_POST['intProcedureType']) > 0) && 
			(strlen($_POST['strProcedureTitle']))) {
			$intCurID				= intval($_POST['pid']);
			$intCurType				= intval($_POST['intProcedureType']);
			$strCurTitle			= stripslashes($_POST['strProcedureTitle']);
			$txtCurDescription		= stripslashes($_POST['txtProcedureDesc']);
			$dblCurFeeAnesthesia	= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeAnesthesia']));
			$dblCurFeeFacility		= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeFacility']));
			$dblCurFeeSurgeon		= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeSurgeon']));
			$dblCurFeeTotal			= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeTotal']));
			
			if (strlen($txtCurDescription) == 0) {
				$txtCurDescription		= $strCurTitle;
			}
			
			$strQuery				= sprintf(	"	UPDATE 	%sapb_concierge_procedures 
													SET		intType				= %d, 
															strTitle			= '%s', 
															txtDescription		= '%s', 
															dblFeeAnesthesia	= %f, 
															dblFeeFacility		= %f, 
															dblFeeSurgeon		= %f, 
															dblFeeTotal			= %f 
													WHERE	intID				= %d", 
												$this->db->prefix, 
												intval($intCurType), 
												$this->db->escape($strCurTitle), 
												$this->db->escape($txtCurDescription), 
												floatval($dblCurFeeAnesthesia), 
												floatval($dblCurFeeFacility), 
												floatval($dblCurFeeSurgeon), 
												floatval($dblCurFeeTotal), 
												intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
	
	public function insert() {
		if ((isset($_POST['intProcedureType'])) && 
			(isset($_POST['strProcedureTitle'])) && 
			(isset($_POST['txtProcedureDesc'])) && 
			(isset($_POST['dblProcedureFeeAnesthesia'])) && 
			(isset($_POST['dblProcedureFeeFacility'])) && 
			(isset($_POST['dblProcedureFeeSurgeon'])) && 
			(isset($_POST['dblProcedureFeeTotal'])) && 
			(intval($_POST['intProcedureType']) > 0) && 
			(strlen($_POST['strProcedureTitle']))) {
			$intCurType				= intval($_POST['intProcedureType']);
			$strCurTitle			= stripslashes($_POST['strProcedureTitle']);
			$txtCurDescription		= stripslashes($_POST['txtProcedureDesc']);
			$dblCurFeeAnesthesia	= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeAnesthesia']));
			$dblCurFeeFacility		= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeFacility']));
			$dblCurFeeSurgeon		= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeSurgeon']));
			$dblCurFeeTotal			= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeTotal']));

			if (strlen($txtCurDescription) == 0) {
				$txtCurDescription		= $strCurTitle;
			}
			
			$strQuery				= sprintf(	"INSERT INTO %sapb_concierge_procedures (intType, strTitle, txtDescription, dblFeeAnesthesia, dblFeeFacility, dblFeeSurgeon, dblFeeTotal) VALUES (%d, '%s', '%s', %f, %f, %f, %f)", 
												$this->db->prefix, 
												intval($intCurType), 
												$this->db->escape($strCurTitle), 
												$this->db->escape($txtCurDescription), 
												floatval($dblCurFeeAnesthesia), 
												floatval($dblCurFeeFacility), 
												floatval($dblCurFeeSurgeon), 
												floatval($dblCurFeeTotal));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
	
	public function remove() {
		if ((isset($_REQUEST['pid'])) && 
			(intval($_REQUEST['pid']) > 0)) {
			$intCurID			= intval($_REQUEST['pid']);
			
			$strQuery			= sprintf(	"UPDATE %sapb_concierge_appointments SET intProcedureID = 0 WHERE intProcedureID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);

			$strQuery			= sprintf(	"DELETE FROM %sapb_concierge_procedures WHERE intID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
}

?>