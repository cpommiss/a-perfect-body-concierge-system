<?php

class APB_Concierge_Admin_Markets {
	const APB_MODULE_SLUG		= 'concierge-markets';
	const APB_MODULE_TITLE		= 'Markets';

	private $db;
	
	function __construct() {
		global $wpdb;
		
		$this->db			= &$wpdb;
	}
	
	public function index() {
		$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_markets ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetMarkets	= $this->db->get_results($strQuery);

		include dirname(__FILE__) . '/../_views/admin-markets-list.phtml';
	}

	public function edit() {
		if ((isset($_REQUEST['mid'])) && (intval($_REQUEST['mid']) > 0)) {
			$intCurAccountID	= intval($_REQUEST['mid']);
			
			$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_markets WHERE intID = %d LIMIT 1", 
											$this->db->prefix, 
											intval($intCurAccountID));
			$queryGetMarket		= $this->db->get_results($strQuery);
			
			if (count($queryGetMarket)) {
				$objRow					= $queryGetMarket[0];
				$intCurMarketID			= intval($objRow->intID);
				$strCurMarketName		= stripslashes($objRow->strName);
				
				include dirname(__FILE__) . '/../_views/admin-markets-edit.phtml';
			} else {
				$this->index();
			}
		} else {
			$this->index();
		}
	}
	
	public function add() {
		include dirname(__FILE__) . '/../_views/admin-markets-add.phtml';
	}

	public function commit() {
		if ((isset($_POST['mid'])) && 
			(isset($_POST['strMarketName'])) && 
			(intval($_POST['mid']) > 0) && 
			(strlen($_POST['strMarketName']))) {
			$intCurID				= intval($_POST['mid']);
			$strCurName				= stripslashes($_POST['strMarketName']);
			
			$strQuery				= sprintf(	"	UPDATE 	%sapb_concierge_markets 
													SET		strName			= '%s' 
													WHERE	intID			= %d", 
												$this->db->prefix, 
												$this->db->escape($strCurName), 
												intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
	
	public function insert() {
		if ((isset($_POST['strMarketName'])) && 
			(strlen($_POST['strMarketName']))) {
			$strCurName				= stripslashes($_POST['strMarketName']);
			
			$strQuery				= sprintf(	"INSERT INTO %sapb_concierge_markets (strName) VALUES ('%s')", 
												$this->db->prefix, 
												$this->db->escape($strCurName));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
	
	public function remove() {
		if ((isset($_REQUEST['mid'])) && 
			(intval($_REQUEST['mid']) > 0)) {
			$intCurID			= intval($_REQUEST['mid']);
			
			$strQuery			= sprintf(	"UPDATE %sapb_concierge_appointments SET intMarketID = 0 WHERE intMarketID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);

			$strQuery			= sprintf(	"DELETE FROM %sapb_concierge_markets WHERE intID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
}

?>