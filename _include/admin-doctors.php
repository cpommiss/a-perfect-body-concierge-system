<?php

class APB_Concierge_Admin_Doctors {
	const APB_MODULE_SLUG		= 'concierge-doctors';
	const APB_MODULE_TITLE		= 'Doctors';

	private $db;
	
	function __construct() {
		global $wpdb;
		
		$this->db			= &$wpdb;
	}
	
	public function index() {
		$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_doctors ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetDoctors	= $this->db->get_results($strQuery);

		include dirname(__FILE__) . '/../_views/admin-doctors-list.phtml';
	}

	public function edit() {
		if ((isset($_REQUEST['did'])) && (intval($_REQUEST['did']) > 0)) {
			$intCurDoctorID		= intval($_REQUEST['did']);
			
			$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_doctors WHERE intID = %d LIMIT 1", 
											$this->db->prefix, 
											intval($intCurDoctorID));
			$queryGetDoctor		= $this->db->get_results($strQuery);
			
			if (count($queryGetDoctor)) {
				$objRow					= $queryGetDoctor[0];
				$intCurDoctorID			= intval($objRow->intID);
				$strCurDoctorName		= stripslashes($objRow->strName);
				
				include dirname(__FILE__) . '/../_views/admin-doctors-edit.phtml';
			} else {
				$this->index();
			}
		} else {
			$this->index();
		}
	}
	
	public function add() {
		include dirname(__FILE__) . '/../_views/admin-doctors-add.phtml';
	}

	public function commit() {
		if ((isset($_POST['did'])) && 
			(isset($_POST['strDoctorName'])) && 
			(intval($_POST['did']) > 0) && 
			(strlen($_POST['strDoctorName']))) {
			$intCurID				= intval($_POST['did']);
			$strCurName				= stripslashes($_POST['strDoctorName']);
			
			$strQuery				= sprintf(	"	UPDATE 	%sapb_concierge_doctors 
													SET		strName			= '%s' 
													WHERE	intID			= %d", 
												$this->db->prefix, 
												$this->db->escape($strCurName), 
												intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
	
	public function insert() {
		if ((isset($_POST['strDoctorName'])) && 
			(strlen($_POST['strDoctorName']))) {
			$strCurName				= stripslashes($_POST['strDoctorName']);
			
			$strQuery				= sprintf(	"INSERT INTO %sapb_concierge_doctors (strName) VALUES ('%s')", 
												$this->db->prefix, 
												$this->db->escape($strCurName));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
	
	public function remove() {
		if ((isset($_REQUEST['did'])) && 
			(intval($_REQUEST['did']) > 0)) {
			$intCurID			= intval($_REQUEST['did']);
			
			$strQuery			= sprintf(	"UPDATE %sapb_concierge_appointments SET intDoctorID = 0 WHERE intDoctorID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);

			$strQuery			= sprintf(	"DELETE FROM %sapb_concierge_doctors WHERE intID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
}

?>