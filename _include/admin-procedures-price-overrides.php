<?php

class APB_Concierge_Admin_Procedures_Price_Overrides {
	const APB_MODULE_SLUG		= 'concierge-procedure-price-override';
	const APB_MODULE_TITLE		= 'Procedure Price Overrides';

	private $db;
	
	function __construct() {
		global $wpdb;
		
		$this->db			= &$wpdb;
	}
	
	public function index() {
		$strQuery			= sprintf(	"	SELECT o.*, t.strName AS strProcedureType, p.strTitle AS strProcedureTitle 
											FROM %sapb_concierge_procedures_price_overrides o 
											LEFT JOIN %sapb_concierge_procedures p ON o.intProcedureID = p.intID 
											LEFT JOIN %sapb_concierge_procedures_types t ON p.intType = t.intID", 
										$this->db->prefix, 
										$this->db->prefix, 
										$this->db->prefix);
		$queryGetOverrides	= $this->db->get_results($strQuery);

		$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_doctors ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetDoctors	= $this->db->get_results($strQuery);

		include dirname(__FILE__) . '/../_views/admin-procedures-price-overrides-list.phtml';
	}

	public function edit() {
		// common datasets
		$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_procedures ORDER BY strTitle ASC", 
										$this->db->prefix);
		$queryGetProcedures	= $this->db->get_results($strQuery);

		$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_doctors ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetDoctors	= $this->db->get_results($strQuery);

		if ((isset($_REQUEST['oid'])) && (intval($_REQUEST['oid']) > 0)) {
			$intCurProcedureID	= intval($_REQUEST['oid']);
			
			$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_procedures_price_overrides WHERE intID = %d LIMIT 1", 
											$this->db->prefix, 
											intval($intCurProcedureID));
			$queryGetOverride	= $this->db->get_results($strQuery);

			if (count($queryGetOverride)) {
				$objRow						= $queryGetOverride[0];
				$intCurOverrideID			= intval($objRow->intID);
				$intCurOverrideProcedureID	= intval($objRow->intProcedureID);
				$intCurOverrideDoctorID		= intval($objRow->intDoctorID);
				$dblCurFeeAnesthesia		= floatval($objRow->dblFeeAnesthesia);
				$dblCurFeeFacility			= floatval($objRow->dblFeeFacility);
				$dblCurFeeSurgeon			= floatval($objRow->dblFeeSurgeon);
				$dblCurFeeTotal				= floatval($objRow->dblFeeTotal);
				
				include dirname(__FILE__) . '/../_views/admin-procedures-price-overrides-edit.phtml';
			} else {
				$this->index();
			}
		} else {
			$this->index();
		}
	}
	
	public function add() {
		// common datasets
		$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_procedures ORDER BY strTitle ASC", 
										$this->db->prefix);
		$queryGetProcedures	= $this->db->get_results($strQuery);

		$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_doctors ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetDoctors	= $this->db->get_results($strQuery);

		include dirname(__FILE__) . '/../_views/admin-procedures-price-overrides-add.phtml';
	}

	public function commit() {
		if ((isset($_POST['oid'])) && 
			(isset($_POST['intProcedureID'])) && 
			(isset($_POST['intProcedureDoctorID'])) && 
			(isset($_POST['dblProcedureFeeAnesthesia'])) && 
			(isset($_POST['dblProcedureFeeFacility'])) && 
			(isset($_POST['dblProcedureFeeSurgeon'])) && 
			(isset($_POST['dblProcedureFeeTotal'])) && 
			(intval($_POST['oid']) > 0) && 
			(intval($_POST['intProcedureID']) > 0) && 
			(intval($_POST['intProcedureDoctorID']) > 0)) {
			$intCurID				= intval($_POST['oid']);
			$intCurProcedureID		= intval($_POST['intProcedureID']);
			$intCurDoctorID			= intval($_POST['intProcedureDoctorID']);
			$dblCurFeeAnesthesia	= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeAnesthesia']));
			$dblCurFeeFacility		= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeFacility']));
			$dblCurFeeSurgeon		= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeSurgeon']));
			$dblCurFeeTotal			= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeTotal']));
			
			$strQuery				= sprintf(	"	UPDATE 	%sapb_concierge_procedures_price_overrides 
													SET		intProcedureID		= %d, 
															intDoctorID			= %d, 
															dblFeeAnesthesia	= %f, 
															dblFeeFacility		= %f, 
															dblFeeSurgeon		= %f, 
															dblFeeTotal			= %f 
													WHERE	intID				= %d", 
												$this->db->prefix, 
												intval($intCurProcedureID), 
												intval($intCurDoctorID), 
												floatval($dblCurFeeAnesthesia), 
												floatval($dblCurFeeFacility), 
												floatval($dblCurFeeSurgeon), 
												floatval($dblCurFeeTotal), 
												intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
	
	public function insert() {
		if ((isset($_POST['intProcedureID'])) && 
			(isset($_POST['intProcedureDoctorID'])) && 
			(isset($_POST['dblProcedureFeeAnesthesia'])) && 
			(isset($_POST['dblProcedureFeeFacility'])) && 
			(isset($_POST['dblProcedureFeeSurgeon'])) && 
			(isset($_POST['dblProcedureFeeTotal'])) && 
			(intval($_POST['intProcedureID']) > 0) && 
			(intval($_POST['intProcedureDoctorID']) > 0)) {
			$intCurProcedureID		= intval($_POST['intProcedureID']);
			$intCurDoctorID			= intval($_POST['intProcedureDoctorID']);
			$dblCurFeeAnesthesia	= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeAnesthesia']));
			$dblCurFeeFacility		= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeFacility']));
			$dblCurFeeSurgeon		= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeSurgeon']));
			$dblCurFeeTotal			= floatval(preg_replace("/[^0-9\.\-]+/", "", $_POST['dblProcedureFeeTotal']));
			
			$strQuery				= sprintf(	"DELETE FROM %sapb_concierge_procedures_price_overrides WHERE intProcedureID = %d AND intDoctorID = %d", 
												$this->db->prefix, 
												intval($intCurProcedureID), 
												intval($intCurDoctorID));
			$this->db->query($strQuery);
												
			$strQuery				= sprintf(	"INSERT INTO %sapb_concierge_procedures_price_overrides (intProcedureID, intDoctorID, dblFeeAnesthesia, dblFeeFacility, dblFeeSurgeon, dblFeeTotal) VALUES (%d, %d, %f, %f, %f, %f)", 
												$this->db->prefix, 
												intval($intCurProcedureID), 
												intval($intCurDoctorID), 
												floatval($dblCurFeeAnesthesia), 
												floatval($dblCurFeeFacility), 
												floatval($dblCurFeeSurgeon), 
												floatval($dblCurFeeTotal));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
	
	public function remove() {
		if ((isset($_REQUEST['oid'])) && 
			(intval($_REQUEST['oid']) > 0)) {
			$intCurID			= intval($_REQUEST['oid']);
			
			$strQuery			= sprintf(	"UPDATE %sapb_concierge_appointments SET intProcedureID = 0 WHERE intProcedureID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);

			$strQuery			= sprintf(	"DELETE FROM %sapb_concierge_procedures WHERE intID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
}

?>