<?php

class APB_Concierge_Admin_Accounts {
	const APB_MODULE_SLUG		= 'concierge';
	const APB_MODULE_TITLE		= 'Concierge';

	private $db;
	
	function __construct() {
		global $wpdb;
		
		$this->db			= &$wpdb;
	}
	
	public function index() {
		$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_accounts ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetAccounts	= $this->db->get_results($strQuery);

		$strQuery			= sprintf(	"SELECT a.intAccountID, a.intMarketID, m.strName AS strMarketName FROM %sapb_concierge_accounts_markets a LEFT JOIN %sapb_concierge_markets m ON a.intMarketID = m.intID ORDER BY m.strName ASC", 
										$this->db->prefix, 
										$this->db->prefix);
		$queryGetMarkets	= $this->db->get_results($strQuery);
		
		include dirname(__FILE__) . '/../_views/admin-accounts-list.phtml';
	}

	public function edit() {
		// common datasets
		$strQuery			= sprintf(	"SELECT intID, strName FROM %sapb_concierge_markets ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetAllMarkets	= $this->db->get_results($strQuery);
		
		if ((isset($_REQUEST['cid'])) && (intval($_REQUEST['cid']) > 0)) {
			$intCurAccountID	= intval($_REQUEST['cid']);
			
			$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_accounts WHERE intID = %d LIMIT 1", 
											$this->db->prefix, 
											intval($intCurAccountID));
			$queryGetAccount	= $this->db->get_results($strQuery);
			
			if (count($queryGetAccount)) {
				$strQuery				= sprintf(	"SELECT intMarketID FROM %sapb_concierge_accounts_markets WHERE intAccountID = %d", 
													$this->db->prefix, 
													intval($intCurAccountID));
				$queryGetMarkets		= $this->db->get_results($strQuery);
				
				$objRow					= $queryGetAccount[0];
				$intCurAccountID		= intval($objRow->intID);
				$intCurAccountType		= intval($objRow->intType);
				$strCurAccountName		= stripslashes($objRow->strName);
				$strCurAccountUsername	= stripslashes($objRow->strUsername);
				$arrCurAccountMarkets	= array();
				
				foreach ($queryGetMarkets as $objRow) {
					$intCurMarketID			= intval($objRow->intMarketID);
					
					array_push($arrCurAccountMarkets, $intCurMarketID);
				}
				
				include dirname(__FILE__) . '/../_views/admin-accounts-edit.phtml';
			} else {
				$this->index();
			}
		} else {
			$this->index();
		}
	}
	
	public function add() {
		// common datasets
		$strQuery			= sprintf(	"SELECT intID, strName FROM %sapb_concierge_markets ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetAllMarkets	= $this->db->get_results($strQuery);
		
		include dirname(__FILE__) . '/../_views/admin-accounts-add.phtml';
	}

	public function commit() {
		// common datasets
		$strQuery			= sprintf(	"SELECT intID, strName FROM %sapb_concierge_markets ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetAllMarkets	= $this->db->get_results($strQuery);
		
		if ((isset($_POST['cid'])) && 
			(isset($_POST['intConciergeType'])) && 
			(isset($_POST['strConciergeUsername'])) && 
			(isset($_POST['strConciergePassword'])) && 
			(isset($_POST['strConciergePasswordV'])) && 
			(isset($_POST['strConciergeName'])) && 
			(intval($_POST['cid']) > 0) && 
			(intval($_POST['intConciergeType']) > 0) && 
			(strlen($_POST['strConciergeUsername'])) && 
			(strlen($_POST['strConciergeName']))) {
			$intCurID				= intval($_POST['cid']);
			$intCurType				= intval($_POST['intConciergeType']);
			$strCurUsername			= stripslashes($_POST['strConciergeUsername']);
			$strCurName				= stripslashes($_POST['strConciergeName']);
			$strCurPassword			= stripslashes($_POST['strConciergePassword']);
			$strCurPasswordV		= stripslashes($_POST['strConciergePasswordV']);
			
			$strQuery				= sprintf(	"	UPDATE 	%sapb_concierge_accounts 
													SET		intType			= %d, 
															strName			= '%s', 
															strUsername		= '%s' 
													WHERE	intID			= %d", 
												$this->db->prefix, 
												intval($intCurType), 
												$this->db->escape($strCurName), 
												$this->db->escape($strCurUsername), 
												intval($intCurID));
			$this->db->query($strQuery);
			
			if ((strlen($strCurPassword)) && (strlen($strCurPasswordV)) && ($strCurPassword == $strCurPasswordV)) {
				$strQuery				= sprintf(	"	UPDATE 	%sapb_concierge_accounts 
														SET		strPassword		= '%s' 
														WHERE	intID			= %d", 
													$this->db->prefix, 
													$this->db->escape($strCurPassword), 
													intval($intCurID));
				$this->db->query($strQuery);
			}
			
			$strQuery					= sprintf(	"DELETE FROM %sapb_concierge_accounts_markets WHERE intAccountID = %d", 
													$this->db->prefix, 
													intval($intCurID));
			$this->db->query($strQuery);
			
			foreach ($queryGetAllMarkets as $objRow) {
				$intCurMarketID				= intval($objRow->intID);
				
				if (isset($_POST['chkConciergeMarket_' . $intCurMarketID])) {
					$strQuery					= sprintf(	"INSERT INTO %sapb_concierge_accounts_markets (intAccountID, intMarketID) VALUES (%d, %d)", 
															$this->db->prefix, 
															intval($intCurID), 
															intval($intCurMarketID));
					$this->db->query($strQuery);
				}
			}
		}
		
		$this->index();
	}
	
	public function insert() {
		// common datasets
		$strQuery			= sprintf(	"SELECT intID, strName FROM %sapb_concierge_markets ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetAllMarkets	= $this->db->get_results($strQuery);
		
		if ((isset($_POST['intConciergeType'])) && 
			(isset($_POST['strConciergeUsername'])) && 
			(isset($_POST['strConciergePassword'])) && 
			(isset($_POST['strConciergePasswordV'])) && 
			(isset($_POST['strConciergeName'])) && 
			(intval($_POST['intConciergeType']) > 0) && 
			(strlen($_POST['strConciergeUsername'])) && 
			(strlen($_POST['strConciergePassword'])) && 
			(strlen($_POST['strConciergePasswordV'])) && 
			(strlen($_POST['strConciergeName'])) && 
			($_POST['strConciergePassword'] == $_POST['strConciergePasswordV'])) {
			$intCurType				= intval($_POST['intConciergeType']);
			$strCurUsername			= stripslashes($_POST['strConciergeUsername']);
			$strCurPassword			= stripslashes($_POST['strConciergePassword']);
			$strCurPasswordV		= stripslashes($_POST['strConciergePasswordV']);
			$strCurName				= stripslashes($_POST['strConciergeName']);
			
			$strQuery				= sprintf(	"INSERT INTO %sapb_concierge_accounts (intType, strName, strUsername, strPassword) VALUES (%d, '%s', '%s', '%s')", 
												$this->db->prefix, 
												intval($intCurType), 
												$this->db->escape($strCurName), 
												$this->db->escape($strCurUsername), 
												$this->db->escape($strCurPassword));
			$this->db->query($strQuery);
			
			$intNewAccountID		= $this->db->insert_id;
			
			if ($intNewAccountID > 0) {
				foreach ($queryGetAllMarkets as $objRow) {
					$intCurMarketID				= intval($objRow->intID);
					
					if (isset($_POST['chkConciergeMarket_' . $intCurMarketID])) {
						$strQuery					= sprintf(	"INSERT INTO %sapb_concierge_accounts_markets (intAccountID, intMarketID) VALUES (%d, %d)", 
																$this->db->prefix, 
																intval($intNewAccountID), 
																intval($intCurMarketID));
						$this->db->query($strQuery);
					}
				}
			}
		}
		
		$this->index();
	}
	
	public function remove() {
		if ((isset($_REQUEST['cid'])) && 
			(intval($_REQUEST['cid']) > 0)) {
			$intCurID			= intval($_REQUEST['cid']);
			
			$strQuery			= sprintf(	"DELETE FROM %sapb_concierge_accounts_markets WHERE intAccountID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);

			$strQuery			= sprintf(	"UPDATE %sapb_concierge_appointments SET intConciergeID = 1 WHERE intConciergeID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);

			$strQuery			= sprintf(	"DELETE FROM %sapb_concierge_accounts WHERE intID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
}

?>