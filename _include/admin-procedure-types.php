<?php

class APB_Concierge_Admin_ProcedureTypes {
	const APB_MODULE_SLUG		= 'concierge-procedure-types';
	const APB_MODULE_TITLE		= 'Procedure Types';

	private $db;
	
	function __construct() {
		global $wpdb;
		
		$this->db			= &$wpdb;
	}
	
	public function index() {
		$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_procedures_types ORDER BY strName ASC", 
										$this->db->prefix);
		$queryGetTypes		= $this->db->get_results($strQuery);

		include dirname(__FILE__) . '/../_views/admin-procedure-types-list.phtml';
	}

	public function edit() {
		if ((isset($_REQUEST['tid'])) && (intval($_REQUEST['tid']) > 0)) {
			$intCurTypeID		= intval($_REQUEST['tid']);
			
			$strQuery			= sprintf(	"SELECT * FROM %sapb_concierge_procedures_types WHERE intID = %d LIMIT 1", 
											$this->db->prefix, 
											intval($intCurTypeID));
			$queryGetType		= $this->db->get_results($strQuery);
			
			if (count($queryGetType)) {
				$objRow					= $queryGetType[0];
				$intCurTypeID			= intval($objRow->intID);
				$strCurTypeName			= stripslashes($objRow->strName);
				
				include dirname(__FILE__) . '/../_views/admin-procedure-types-edit.phtml';
			} else {
				$this->index();
			}
		} else {
			$this->index();
		}
	}
	
	public function add() {
		include dirname(__FILE__) . '/../_views/admin-procedure-types-add.phtml';
	}

	public function commit() {
		if ((isset($_POST['tid'])) && 
			(isset($_POST['strTypeName'])) && 
			(intval($_POST['tid']) > 0) && 
			(strlen($_POST['strTypeName']))) {
			$intCurID				= intval($_POST['tid']);
			$strCurName				= stripslashes($_POST['strTypeName']);
			
			$strQuery				= sprintf(	"	UPDATE 	%sapb_concierge_procedures_types 
													SET		strName			= '%s' 
													WHERE	intID			= %d", 
												$this->db->prefix, 
												$this->db->escape($strCurName), 
												intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
	
	public function insert() {
		if ((isset($_POST['strTypeName'])) && 
			(strlen($_POST['strTypeName']))) {
			$strCurName				= stripslashes($_POST['strTypeName']);
			
			$strQuery				= sprintf(	"INSERT INTO %sapb_concierge_procedures_types (strName) VALUES ('%s')", 
												$this->db->prefix, 
												$this->db->escape($strCurName));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
	
	public function remove() {
		if ((isset($_REQUEST['tid'])) && 
			(intval($_REQUEST['tid']) > 0)) {
			$intCurID			= intval($_REQUEST['tid']);
			
			$strQuery			= sprintf(	"UPDATE %sapb_concierge_procedures SET intType = 0 WHERE intType = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);

			$strQuery			= sprintf(	"DELETE FROM %sapb_concierge_procedures_types WHERE intID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
}

?>